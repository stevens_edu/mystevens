/**
 * @file
 * Custom scripts for theme.
 */
(function ($) {
jQuery(document).ready(function () {
  var $window = jQuery(window);
  var windowsize = $window.width();
  var breakpoint = 767;
  function checkWidth() {
    if (typeof checkWidth.firstrun == "undefined") {
      checkWidth.firstrun = true;
    }
    if(jQuery(window).width() != windowsize || checkWidth.firstrun === true) {
      var oldwindowsize = windowsize;
      windowsize = jQuery(window).width();
      if(!jQuery("span.additional-nav-handle").length) {
        jQuery("#page-title").after("<span class='additional-nav-handle'><span class='additional-nav-hamburger'>&#9776;&nbsp;</span>Additional Navigation</span><span class='additional-nav-handle-closed'><span class='additional-nav-x'>&#10006;&nbsp;</span>Close</span>");
      }
      if ((oldwindowsize > breakpoint || checkWidth.firstrun === true) && windowsize <= breakpoint) {
        jQuery(".additional-nav-handle").show();
        jQuery(".pane-og-menu-single, .stevens-ctp-menu").hide();
      }
      if (windowsize > breakpoint) {
        jQuery(".pane-og-menu-single, .stevens-ctp-menu").show();
        jQuery(".additional-nav-handle, .additional-nav-handle-closed").hide();
      }
    }
    checkWidth.firstrun = false;
  }
  
  if (jQuery(".pane-og-menu-single").length || jQuery(".stevens-ctp-menu").length) {
    checkWidth();
    jQuery(window).resize(checkWidth);
  }

  jQuery(".additional-nav-handle, .additional-nav-handle-closed").click(function () {
    if (windowsize <= breakpoint) {
      jQuery(".pane-og-menu-single, .stevens-ctp-menu").toggle("slow");
      jQuery(".additional-nav-handle").toggle();
      jQuery(".additional-nav-handle-closed").toggle();
    }
  });
});
})(jQuery);
