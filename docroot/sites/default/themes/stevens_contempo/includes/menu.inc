<?php
/**
 * @file
 * Theme and preprocess functions for menus
 */

/**
 * Implements theme_menu_tree__menu_block().
 */
function stevens_contempo_menu_tree__menu_block(&$variables) {
  return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

