
  jQuery(document).ready(function () {

    // Get the last URI segment (search query string)
    var url = window.location.href;
    function parseUrl(url) {
      var a = document.createElement("a");
      a.href = url;
      return a;
    }
    var parser = parseUrl(url);
    var last_segment = parser.pathname.split('/').pop();

    // Apply tabs to search results
    var tabs = jQuery( "#search-tabs" ).tabs();

    // Set current tab from sessionStorage
    var currentTabIndex = sessionStorage.getItem("currentSearchTabIndex");
    if (jQuery.type(currentTabIndex) != "undefined")
    {
        jQuery( "#search-tabs" ).tabs( "option", "active", currentTabIndex );
    }

    // Record current tab in sessionStorage on tab click event
    jQuery(function() {
      jQuery("ul.tabs li").click(function() {
          var selected = tabs.tabs('option', 'active');
          sessionStorage.setItem("currentSearchTabIndex", selected);
      });
    });

    /**
    * Google Custom Search Integration
    */

    // Hide GSE input
    jQuery("form#google-cse-results-searchbox-form div").css("display", "none");

    // Change External results header text
    jQuery("#block-google-cse-google-cse h2").html("Stevens.edu Search Results");

    // Auto-submit Google search form
    jQuery("#google-cse-results-searchbox-form #edit-query").attr("value", decodeURIComponent(last_segment));

    // TODO:  This timeout is way too hacky.  Need to find an event emitted when the google cse lib is loaded and use a jQuery(document).on('some-event')...
    setTimeout(function(){
      jQuery("form#google-cse-results-searchbox-form").submit();
    },2000)
    // End TODO

  });
