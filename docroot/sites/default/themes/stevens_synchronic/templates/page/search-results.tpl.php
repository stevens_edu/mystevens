<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */
?>
<?php drupal_set_message(t('Please note that the search pages for <a href="https://www.stevens.edu/search">Stevens.edu</a> and <a href="https://www.stevens.edu/peoplefinder">People Finder</a> have moved.')); ?>
<?php if($search_term_exists): ?>
    <div id="mystevens-search-results-container">
      <?php if ($search_results): ?>
        <h2><?php print t('myStevens Search Results');?></h2>
        <ol class="search-results <?php print $module; ?>-results">
          <?php print $search_results; ?>
        </ol>
        <?php print $pager; ?>
      <?php else : ?>
        <h2><?php print t('myStevens Search Results');?></h2>
        <p><strong><?php print t('Your search yielded no results.');?></strong></p>
        <?php print search_help('search#noresults', drupal_help_arg()); ?>
      <?php endif; ?>
    </div>
<?php endif; ?>
