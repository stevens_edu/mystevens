<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul id="directory-list">
  <?php foreach ($rows as $id => $row): ?>
    <li>
      <div class="directory-item">
        <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
          <?php print $row; ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
