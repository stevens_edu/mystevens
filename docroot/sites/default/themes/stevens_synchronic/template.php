<?php
/**
 * @file
 * Theme functions
 */

// Include all files from the includes directory.
$includes_path = dirname(__FILE__) . '/includes/*.inc';
foreach (glob($includes_path) as $filename) {
  require_once dirname(__FILE__) . '/includes/' . basename($filename);
}

/**
 * Implements template_preprocess_page().
 */
function stevens_synchronic_preprocess_page(&$variables) {
  // Add copyright to theme.
  if ($copyright = theme_get_setting('copyright')) {
    $variables['copyright'] = check_markup($copyright['value'], $copyright['format']);
  }
}

/**
 * Implements template_preprocess_views_view(&$vars).
 */
function stevens_synchronic_preprocess_views_view(&$variables) {
  if($variables['name'] == 'directory' && $variables['display_id'] == 'page'){
    $view = $variables['view'];
    $alpha = range('A', 'Z');
    $variables['index_array'] = array_combine($alpha, $alpha);
    array_unshift($variables['index_array'], 'ALL');
    $variables['index_array'] = array_flip($variables['index_array']);
    $variables['index_array']['ALL'] = '';
  }

  if($variables['name'] == 'oa_event_list' && $variables['display_id'] == 'oa_events_upcoming_small') {
    $all_events_btn = '<div class="all-events-btn"><span><a href="/full-calendar">All Events</a></span></div>';
    $pager = $variables['pager'];
    if ($pager) {
      $pager = preg_replace('/\<\/div\>$/s', '', $pager);
      $pager .= $all_events_btn . '</div>';
    }
    else {
      $pager = $all_events_btn;
    }
    $variables['pager'] = $pager;
  }

  if($variables['name'] == 'stevens_mystevens_events_by_channel' && $variables['display_id'] == 'upcoming_events_small') {
    $all_events_btn = '<div class="all-events-btn"><span><a href="/events">All Events</a></span></div>';
    $pager = $variables['pager'];
    if ($pager) {
      $pager = preg_replace('/\<\/div\>$/s', '', $pager);
      $pager .= $all_events_btn . '</div>';
    }
    else {
      $pager = $all_events_btn;
    }
    $variables['pager'] = $pager;
  }
}

/**
 * Implementation of hook_preprocess_oa_user_badge().
 */
function stevens_synchronic_preprocess_oa_user_badge(&$vars) {
  $vars['links']['#links']['dashboard']['title'] = t('My Info');

  global $user;
  if (!in_array('administrator', $user->roles)) {
    unset($vars['links']['#links']['edit_profile']);
  }
}

/**
 * Replacement for theme_webform_element().
 */
function stevens_synchronic_webform_element($variables) {
  $element = $variables['element'];
  $wrapper_div = "<div class=\"webform-prefix-postfix-wrapper\">\n";
  $output = '<div ' . drupal_attributes($element['#wrapper_attributes']) . '>' . "\n";
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . webform_filter_xss($element['#field_prefix']) . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  // Generate description for above or below the field.
  $above = !empty($element['#webform_component']['extra']['description_above']);
  $description = array(
    FALSE => '',
    TRUE => !empty($element['#description']) ? ' <div class="description">' . $element['#description'] . "</div>\n" : '',
  );

  switch ($element['#title_display']) {
    case 'inline':
      $output .= $description[$above];
      $description[$above] = '';
      // FALL THRU.
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $description[$above];
      $output .= $wrapper_div;
      $output .= $prefix . $element['#children'] . $suffix . "\n";
      $output .= "</div>";
      break;

    case 'after':
      $output .= ' ' . $description[$above];
      $output .= $wrapper_div;
      $output .= $prefix . $element['#children'] . $suffix . "\n";
      $output .= "</div>";
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $description[$above];
      $output .= $wrapper_div;
      $output .= $prefix . $element['#children'] . $suffix . "\n";
      $output .= "</div>";
      break;
  }

  $output .= $description[!$above];
  $output .= "</div>\n";

  return $output;
}

/**
* Implements hook_preprocess_search_results().
*/
function stevens_synchronic_preprocess_search_results(&$variables) {

  // Check if a search term exists and pass to the template
  $variables['search_term_exists'] = (arg(2) != "" && arg(2) != NULL) ? TRUE : FALSE;

  // Google CSE Block
  $google_block = block_load('google_cse', 'google_cse');
  $variables['google_form'] = drupal_render(_block_get_renderable_array(_block_render_blocks(array($google_block))));

  // Set search query variable
  $variables['search_query_string'] = arg(2);

  $variables['search_results'] = '';
  if (!empty($variables['module'])) {
    $variables['module'] = check_plain($variables['module']);
  }
  foreach ($variables['results'] as $result) {
    $variables['search_results'] .= theme('search_result', array('result' => $result, 'module' => $variables['module']));
  }
  $variables['pager'] = theme('pager', array('tags' => NULL));
  $variables['theme_hook_suggestions'][] = 'search_results__' . $variables['module'];

  drupal_add_js(libraries_get_path('iframe-resizer')  . '/js/iframeResizer.min.js', array('type' => 'file',
                                                                     'scope' => 'header',
                                                                    ));

  drupal_add_js(path_to_theme() . '/assets/stevens_synchronic.external_search_integrations.js', array('type' => 'file',
                                                                     'scope' => 'footer',
                                                                     'group' =>  1000,
                                                                     'weight' => 99999,
                                                                    ));

}

/**
* Implements hook_apachesolr_search_page_alter().
*/
function stevens_synchronic_apachesolr_search_page_alter(&$build, &$search_page) {
  $build['search_results'] = array(
        '#theme' => 'search_results',
        '#results' => $build['search_results']['#results'],
        '#module' => 'apachesolr_search',
        '#search_page' => $search_page,
      );
}
