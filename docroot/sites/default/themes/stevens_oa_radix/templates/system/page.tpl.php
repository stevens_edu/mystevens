<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>

<?php 
  //set this in template.php?
  $user_is_admin = FALSE;
  global $user;
  if (is_array($user->roles) && in_array('administrator', $user->roles)) {
    $user_is_admin = TRUE;
  }
  $user_is_anon = FALSE;
  if (is_array($user->roles) && in_array('anonymous user', $user->roles)) {
    $user_is_anon = TRUE;
  }


?>
      <header id="header" class="header" role="header">
          <div class="row">
            <div class="col-md-8 radix-layouts-header panel-panel">
              <div class="panel-panel-inner">
                <div class="panel-pane pane-oa-space-banner">
                  <div class="pane-content">
                  
                    <div class="oa-banner oa-banner-before oa-banner-appeared" data-width="0" data-height="58" style="height: auto;">
                      <img class="oa-banner-img" src="/sites/default/files/logo_new.png" width="278" height="58" alt="myStevens - Online Services for the Stevens Community">    </div>
          <?php if($user_is_anon) : ?>
                            <div class="login-cont">
                                <a class="login-link" href="/saml_login">Login</a>
                            </div>
         <?php endif;?>
                    </div>

                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div id="welcome">
                <p>Welcome <?php print $user->realname; ?></p>
                <p><a href="/user/logout">Sign Out</a></p>
              </div>
            </div>
        </div>
      </header>

<div id="main-wrapper">
  <div id="main" class="main container">
    <?php if ($breadcrumb && (arg(0) == 'admin')): ?>
      <div id="breadcrumb" class="visible-desktop">
        <div class="container">
          <?php print $breadcrumb; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($messages): ?>
      <div id="messages">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>
    <div id="content">
      <div class="row">
        <div class="col-md-12 inner">
          <?php //Show tabs only if user is admin ?>
          <?php if($user_is_admin): ?>
            <?php if (!empty($primarytabs)): ?><?php print $primarytabs; ?><?php endif; ?>
          <?php endif; //End admin only block ?>
          <a id="main-content"></a>
          <?php if (!empty($tabs['#primary']) || !empty($tabs['#secondary'])): ?><div class="tabs main-tabs"><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div>
  </div>
</div>
