<?php

function stevens_oa_radix_preprocess_page(&$variables) {

  //If the page is a node and has ID 56
  // NID 56 is the myStevens Landing Page node
  /*if (!empty($variables['node']) && $variables['node']->nid == '56') {
    echo "in prepage.";
    $variables['show_title'] = FALSE;
    $variables['show_oa_bar'] = FALSE;
    $variables['show_oa_footer'] = FALSE;
  }*/
  $variables['oa_toolbar_sticky'] = '';
  drupal_add_js('jQuery(document).ready(function () { jQuery("#panels-ipe-control-panelizer-node-56-page-manager-56").hide(); });', 'inline');
}

function stevens_oa_radix_preprocess_html(&$variables) {
#  print_r($variables);
}

/**
 * Implements hook_js_alter().
 *
 * Remove positioning scripts for OA Toolbar, as it will no longer be fixed.
 */
function stevens_oa_radix_js_alter(&$javascript) {
  unset($javascript['profiles/openatrium/modules/apps/oa_toolbar/js/oa-toolbar.js']);
  unset($javascript['profiles/openatrium/modules/apps/oa_appearance/oa_appearance.js']);
  $file = drupal_get_path('theme', 'stevens_oa_radix') . 'assets/js/navbar.js';
  $javascript['sites/default/modules/contrib/navbar/js/navbar.js'] = drupal_js_defaults($file);
  $file = drupal_get_path('theme', 'stevens_oa_radix') . 'assets/js/oa_appearance.js';
  $javascript['profiles/openatrium/modules/apps/oa_appearance/oa_appearance.js'] = drupal_js_defaults($file);
}

function stevens_oa_radix_preprocess_oa_user_badge(&$vars) {
  global $user;

  if ($user->uid) {
    $user = user_load($user->uid);
    if (module_exists('oa_users')) {
      $vars = array_merge($vars, oa_users_build_user_details($user));

      // Turn the links from oa_users_build_user_details() into a proper render
      // array so that it can be modified.
      $paths = $vars['links'];
      $vars['links'] = array(
        '#theme' => 'links',
        '#links' => array(
          'dashboard' => array(
            'title'  => t('Dashboard'),
            'href'   => $paths['dashboard'],
            'weight' => -10,
          ),
          'edit_profile' => array(
            'title'  => t('Edit profile'),
            'href'   => $paths['edit_profile'],
            'weight' => -9,
          ),
          'logout' => array(
            'title'  => t('Log out'),
            'href'   => $paths['logout'],
            'weight' => 50,
          ),
        ),
      );
    }
  }
  else {
    $vars['login'] = url('saml_login');
  }
  $vars['oa_toolbar_btn_class'] = variable_get('oa_toolbar_style', 0) ? '' : 'btn-inverse';
}


//  Implements hook_css_alter
//  to remove colorizer database driven CSS file inclusion
function stevens_oa_radix_css_alter(&$css) {
  $exclude = "/colorizer/";
  $foundIndices = array();
  foreach ($css as $stylesheet => $values) {
    if (strpos($stylesheet, $exclude)) {
      $foundIndices[$stylesheet] = TRUE;
    }
  }
  $css = array_diff_key($css, $foundIndices);
}


// Implements hook_html_head_alter to unset the
// the bootstrap_responsive in $head_elements variable
function stevens_oa_radix_html_head_alter(&$head_elements) {
  unset($head_elements['bootstrap_responsive']);
}
