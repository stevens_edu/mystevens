// This fix reenables the expanding/collapsing of the mobile menu
// when the hamburger nav item is clicked.
jQuery( document ).ready(function() {
  jQuery('.navbar-toggle').click(function() {
    jQuery('.panel-pane.pane-oa-navigation').slideToggle("slow");
  });
});
