/**
 * @file
 * Custom scripts for theme.
 */
(function ($) {
  // code here
  $(document).ready(function() {

    //Auto-expand all Level 2 menu items which have children
    jQuery(".has-children.level-2").addClass("start-expanded").removeClass("start-collapsed");
    jQuery(".has-children.level-2").addClass("expanded").removeClass("collapsed");

    jQuery(".has-children").find("a").click(function() {
      jQuery(".menu-expanded",jQuery(this).parent()).toggle();
      jQuery(".menu-expandable",jQuery(this).parent()).toggle();
    });
  });
})(jQuery);
