<?php
/**
 * @file
 * Theme functions
 */

require_once dirname(__FILE__) . '/includes/structure.inc';
require_once dirname(__FILE__) . '/includes/form.inc';
require_once dirname(__FILE__) . '/includes/menu.inc';
require_once dirname(__FILE__) . '/includes/comment.inc';
require_once dirname(__FILE__) . '/includes/node.inc';

/**
 * Implements template_preprocess_search_result()
 */
function stevens_intranet_preprocess_search_result(&$variables) {

  // Get the entity ID of this search reseult
  $entity_id = $variables['result']['fields']['entity_id'];

  // Get OG Memberships
  $node = node_load($entity_id);

  $output = "";
  foreach (og_get_entity_groups('node', $node) as $entity_type => $og_memberships) {
    foreach ($og_memberships as $membership_id => $group_entity_id) {
      if ($entity_type == 'node') {
        // Get the Group Node
        $group_node = node_load($group_entity_id);

        // Ignore group if it used only for access control
        $non_space_groups = array("staff" => 7941, "faculty" => 7936);
        if(!in_array($group_node->nid, $non_space_groups)) {
          $output .= " | " . l($group_node->title, "node/" . $group_node->nid);
        }
      }
    }
  }
  
  if ($output != ""){
    $output = "Space: " . ltrim($output, ' | ');
  }
    $variables['info'] =  $output;
}

/**
 * Implements hook_css_alter().
 */
function stevens_intranet_css_alter(&$css) {
  $oa_radix_path = drupal_get_path('theme', 'oa_radix');

  // Radix now includes compiled stylesheets for demo purposes.
  // We remove these from our subtheme since they are already included
  // in compass_radix.
  unset($css[$oa_radix_path . '/assets/stylesheets/oa_radix-style.css']);
  unset($css[$oa_radix_path . '/assets/stylesheets/oa_radix-print.css']);
}

/**
 * Implements template_preprocess_page().
 */
function stevens_intranet_preprocess_page(&$vars) {
  // Add copyright to theme.
  if ($copyright = theme_get_setting('copyright')) {
    $vars['copyright'] = check_markup($copyright['value'], $copyright['format']);
  }

  // Rework search_form to our liking.                                                                           
  $vars['search_form'] = '';                                                                                     
  if (module_exists('search') && user_access('search content')) {                                                
    $search_box_form = drupal_get_form('search_form');                                                           
    $search_box_form['basic']['keys']['#title'] = '';                                                            
    $search_box_form['basic']['keys']['#attributes'] = array('placeholder' => 'Search');                         
    $search_box_form['basic']['keys']['#attributes']['class'][] = 'search-query';                                
    $search_box_form['basic']['submit']['#value'] = t('Search');                                                 
    $search_box_form['#attributes']['class'][] = 'navbar-form';                                                  
    $search_box_form['#attributes']['class'][] = 'pull-right';                                                   
    $search_box = drupal_render($search_box_form);                                                               
    $vars['search_form'] = (user_access('search content')) ? $search_box : NULL;                                 
  }                                                                                                              
  $vars['search_form'] = '';
                                                                                                                 
  $vars['user_badge'] = '';                                                                                      
  $vars['mobile_buttons'] = '';                                                                                  
  if (module_exists('oa_toolbar')) {                                                                             
    // Add user_badge to header.                                                                                 
    $user_badge = module_invoke('oa_toolbar', 'block_view', 'oa_user_badge');                                    
    $vars['user_badge'] = $user_badge['content'];                                                                
    $vars['mobile_buttons'] = theme('oa_toolbar_mobile');                                                        
  }                                                                                                              
  $toolbar_name = variable_get_value('oa_layouts_minipanel_header', array('defaults' => 'oa_toolbar_panel'));    
  $toolbar = panels_mini_block_view($toolbar_name);                                                              
  $vars['oa_toolbar_panel'] = isset($toolbar) ? $toolbar['content'] : '';                                        
  $footer_name = variable_get_value('oa_layouts_minipanel_footer', array('defaults' => 'oa_footer_panel'));      
  $footer = panels_mini_block_view($footer_name);                                                                
  $vars['oa_footer_panel'] = isset($footer) ? $footer['content'] : '';                                           
                                                                                                                 
  ctools_include('content');                                                                                     
  $banner = ctools_content_render('oa_space_banner', '', array(                                                  
    'banner_position' => 2                                                                                       
  ));                                                                                                            
  if (!empty($banner->content)) {                                                                                
    echo "<pre>";
    //var_dump($banner->content);
    echo "</pre>";
    $vars['oa_banner'] = $banner->content;                                                                       
  }                                                                                                              
  $vars['oa_space_menu'] = '';                                                                                   
  $space_id = oa_core_get_space_context();                                                                       
  if (variable_get('oa_space_menu_' . $space_id, TRUE)) {                                                        
    $space_menu = ctools_content_render('oa_space_menu', '', array(), array());                                  
    if (!empty($space_menu->content)) {                                                                          
      $vars['oa_space_menu'] = $space_menu->content;                                                             
    }                                                                                                            
  }
}

/**
 * Implements template_preprocess_oa_navigation().
 */
function stevens_intranet_preprocess_oa_navigation(&$variables) {

  // Get the mlid and plids for this path
  $mlid = array();                                   
  $q = $_GET['q'];                                   
  $menu_info = db_select('menu_links' , 'ml')        
    ->condition('ml.link_path' , $q)                 
    ->fields('ml', array('mlid', 'plid'))            
    ->execute()                                      
    ->fetchAll();                                    
                                                   
  // Sort into MLID and PLID arrays
  foreach ($menu_info as $key => $value) {           
    $mlids[] = $menu_info[$key]->mlid;               
    $plids[] = $menu_info[$key]->plid;               
  }                                                  

  // Get the MLIDs that are to be loaded on this page, filter out non-numeric
  // elements that are used in rendering.
  foreach ($variables['menu'] as $mlid => $menu) {
    if (is_numeric($mlid)) {
      $all_mlids[] = $mlid;
    }
  }
                                                   
  // Combine MLIDs and PLIDs into one array and dedupe
  $allowed_mlids = array_merge($mlids, $plids);     
  $allowed_mlids = array_unique($allowed_mlids);
  
  // For all MLIDs currently allowed, get all _their_ PLIDs
  // and build the master "allowed" list.
  // Be sure to maintain the source (child) MLID in the list.
  $all_allowed_mlids = array();
  foreach ($allowed_mlids as $mlid) {
    $menuParents = token_menu_link_load_all_parents($mlid);
    foreach ($menuParents as $plid => $name) {
      $all_allowed_mlids[] = (int)$plid;
    }
    $all_allowed_mlids[] = (int)$mlid;
  }

  // Dedupe master allowed list
  $all_allowed_mlids = array_unique($all_allowed_mlids);
            
  // Finally, compare the "to be rendered" array to the "allowed" array
  // and unset the MLIDs that don't make the cut.
  foreach ($all_mlids as $key => $mlid) {
    if (!in_array($mlid, $all_allowed_mlids)) {
      unset($variables['menu'][$mlid]);
    }
  }
}

/**
 * Implements theme_menu_link().
 */
function stevens_intranet_menu_link(array $variables) {
  $element = $variables['element'];

  $sub_menu = '';

  $depth = $element['#original_link']['depth'];
  $element['#attributes']['class'][] = 'level-' . $depth;

  $has_children = FALSE;
  if ($element['#original_link']['has_children'] == 1) {
    $element['#attributes']['class'][] = 'has-children';
    $has_children = TRUE;
  }

  $expanded = FALSE;
  if (isset($element['#original_link']['start-expanded']) && $element['#original_link']['start-expanded'] == 1) {
    $expanded = TRUE;
  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  $expandable = '';
  if ($has_children) {
    if($expanded) {
      $expandable = '<div class="menu-expanded" style="display: block;"></div>';
      $expandable .= '<div class="menu-expandable" style="display: none;"></div>';
    }
    else {
      $expandable = '<div class="menu-expandable style="display: block;"></div>';  
      $expandable .= '<div class="menu-expanded" style="display: none;"></div>';
    }
  } 

  $markup = '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $expandable . $sub_menu . "</li>\n";

  return $markup;
}

