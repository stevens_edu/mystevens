<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_mystevens_subscription_channels_taxonomy_default_vocabularies() {
  return array(
    'subscription_channels' => array(
      'name' => 'Subscription Channels',
      'machine_name' => 'subscription_channels',
      'description' => 'The set of channels a user can subscribe to and receive notifications about content tagged accordingly.',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
