<?php

/**
 * @file
 * Page callbacks for the Stevens myStevens Subscription Channels module.
 */

/**
 * Menu callback; subscribes the current user to the specified term.
 *
 * @param $term
 *   The taxonomy term.
 * @return
 *   The page content.
 */
function _stevens_mystevens_subscription_channels_subscribe_page($term) {
  global $user;

  // Create entity metadata wrapper.
  $uwrap = entity_metadata_wrapper('user', $user->uid);

  // Get existing terms.
  $existing_terms = $uwrap->field_subscription_channels->value();
  $existing_tids = array();
  foreach ($existing_terms as $existing_term) {
    $existing_tids[] = $existing_term->tid;
  }

  // Add the current term if it wasn't already there.
  if (!in_array($term->tid, $existing_tids)) {
    $new_terms = $existing_terms;
    $new_terms[] = $term;

    $uwrap->field_subscription_channels = array_values($new_terms);
    $uwrap->save();
    drupal_set_message('You are now subscribed to ' . check_plain($term->name) . '.');
  }
  else {
    // The user clicked a stale Subscribe button. We don't render a Subscribe
    // button (instead, an Unsubscribe button) if already subscribed, but alas
    // the user found one.
    drupal_set_message('You were already subscribed to ' . check_plain($term->name) . '.', 'warning');
  }

  drupal_goto('taxonomy/term/' . $term->tid);
}

/**
 * Menu callback; unsubscribes the current user to the specified term.
 *
 * @param $term
 *   The taxonomy term.
 * @return
 *   The page content.
 */
function _stevens_mystevens_subscription_channels_unsubscribe_page($term) {
  global $user;

  // Create entity metadata wrapper.
  $uwrap = entity_metadata_wrapper('user', $user->uid);

  // Get existing terms.
  $existing_terms = $uwrap->field_subscription_channels->value();

  // Build new terms with all but current term.
  $new_terms = $existing_terms;
  $found = FALSE;
  foreach($existing_terms as $key => $existing_term) {
    if ($existing_term->tid == $term->tid) {
      unset($new_terms[$key]);
      $found = TRUE;
      break;
    }
  }

  if ($found) {
    $uwrap->field_subscription_channels = array_values($new_terms);
    $uwrap->save();
    drupal_set_message('You are now unsubscribed to ' . check_plain($term->name) . '.');
  }
  else {
    // The user clicked a stale Unsubscribe button. We don't render an
    // Unsubscribe button (instead, a Subscribe button) if already subscribed,
    // but alas the user found one.
    drupal_set_message('You were already not subscribed to ' . check_plain($term->name) . '.', 'warning');
  }

  drupal_goto('taxonomy/term/' . $term->tid);
}

/**
 * Menu callback; Leads the user to their Edit Profile page.
 *
 * @return
 *   The page content.
 */
function _stevens_mystevens_subscription_channels_manage_page() {
  global $user;
  drupal_goto('user/' . $user->uid . '/edit');
}
