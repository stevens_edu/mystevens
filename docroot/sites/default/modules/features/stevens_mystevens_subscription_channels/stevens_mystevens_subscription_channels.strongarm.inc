<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function stevens_mystevens_subscription_channels_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_taxonomy_term_subscription_channels';
  $strongarm->value = array(
    'status' => 1,
    'help' => '',
    'view modes' => array(
      'page_manager' => array(
        'status' => 1,
        'substitute' => '',
        'default' => 1,
        'choice' => 0,
      ),
      'default' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
      'featured' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_taxonomy_term_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_taxonomy_term:subscription_channels:default_selection';
  $strongarm->value = 'taxonomy_term:subscription_channels:default:default';
  $export['panelizer_taxonomy_term:subscription_channels:default_selection'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_taxonomy_term:subscription_channels:featured_selection';
  $strongarm->value = 'taxonomy_term:subscription_channels:default:featured';
  $export['panelizer_taxonomy_term:subscription_channels:featured_selection'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_taxonomy_term:subscription_channels:full_selection';
  $strongarm->value = 'taxonomy_term:subscription_channels:default:full';
  $export['panelizer_taxonomy_term:subscription_channels:full_selection'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_taxonomy_term:subscription_channels:page_manager_selection';
  $strongarm->value = 'taxonomy_term:subscription_channels:default';
  $export['panelizer_taxonomy_term:subscription_channels:page_manager_selection'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_display_descendants_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_display_descendants_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_display_num_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_display_num_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_end_all_subscription_channels';
  $strongarm->value = FALSE;
  $export['taxonomy_menu_end_all_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_expanded_subscription_channels';
  $strongarm->value = 1;
  $export['taxonomy_menu_expanded_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_flat_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_flat_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_hide_empty_terms_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_hide_empty_terms_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_path_subscription_channels';
  $strongarm->value = 'taxonomy_menu_path_default';
  $export['taxonomy_menu_path_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_rebuild_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_rebuild_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_sync_subscription_channels';
  $strongarm->value = 1;
  $export['taxonomy_menu_sync_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_term_item_description_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_term_item_description_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_vocab_menu_subscription_channels';
  $strongarm->value = 'menu-channels';
  $export['taxonomy_menu_vocab_menu_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_vocab_parent_subscription_channels';
  $strongarm->value = '0';
  $export['taxonomy_menu_vocab_parent_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_voc_item_description_subscription_channels';
  $strongarm->value = 0;
  $export['taxonomy_menu_voc_item_description_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_voc_item_subscription_channels';
  $strongarm->value = FALSE;
  $export['taxonomy_menu_voc_item_subscription_channels'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_voc_name_subscription_channels';
  $strongarm->value = FALSE;
  $export['taxonomy_menu_voc_name_subscription_channels'] = $strongarm;

  return $export;
}
