<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function stevens_mystevens_subscription_channels_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-channels.
  $menus['menu-channels'] = array(
    'menu_name' => 'menu-channels',
    'title' => 'Channels',
    'description' => 'A menu automatically maintained by the Taxonomy Menu module to mirror the Subscription Channels vocabulary.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A menu automatically maintained by the Taxonomy Menu module to mirror the Subscription Channels vocabulary.');
  t('Channels');

  return $menus;
}
