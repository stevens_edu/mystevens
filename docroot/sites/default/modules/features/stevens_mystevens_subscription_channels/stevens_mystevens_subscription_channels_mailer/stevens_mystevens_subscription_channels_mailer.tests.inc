<?php

function stevens_mystevens_subscription_channels_mailer_run_tests() {
  // TODO:  Create temporary test taxonomy
  // TODO:  Create tempoaray test node
  // TODO:  Create temporary test user
  // TODO:  Associate test node to terms in test vocabular via term reference on node
  // TODO:  Associate test user to test node via term reference on profile
  
  // Check for stevens_mystevens_subscription_channels_get_recipient_uids_from_nid().
  $expected_recipient_uids_from_nid = array("11" => "11",
                                            "16" => "16",
                                            "31" => "31",
                                            );
  $recipient_uids_from_nid = stevens_mystevens_subscription_channels_mailer_get_recipient_uids_from_nid(41926);
  dsm("Expected UIDs from stevens_mystevens_subscription_channels_get_recipient_uids_from_nid() call: ");
  dsm($expected_recipient_uids_from_nid);
  dsm("Returned UIDs from stevens_mystevens_subscription_channels_get_recipient_uids_from_nid() call: ");
  dsm($recipient_uids_from_nid);

  if($recipient_uids_from_nid == $expected_recipient_uids_from_nid) {
    dsm ("stevens_mystevens_subscription_channels_get_recipient_uids_from_nid() = PASS");
  }
  else {
    dsm ("stevens_mystevens_subscription_channels_get_recipient_uids_from_nid() = FAIL");
  }

  // Check for _stevens_mystevens_subscription_channels_get_recipient_tids_from_nid().
  $expected_recipient_tids_from_nid = array("241" => "Training Section",
                                            "251" => "Office Hours Section",
                                            "246" => "Blog Section",
                                            "201" => "Announcement Section",
                                            "31" => "Tasks Section",
                                            "231" => "Promotional Area Section",
                                            "236" => "Contact Us Section",
                                            "216" => "Who We Are Section",
                                            );
  $recipient_tids_from_nid = _stevens_mystevens_subscription_channels_mailer_get_recipient_tids_from_nid(41926);
  dsm("Expected UIDs from _stevens_mystevens_subscription_channels_get_recipient_tids_from_nid() call: ");
  dsm($expected_recipient_tids_from_nid);
  dsm("Returned UIDs from _stevens_mystevens_subscription_channels_get_recipient_tids_from_nid() call: ");
  dsm($recipient_tids_from_nid);

  if($recipient_tids_from_nid == $expected_recipient_tids_from_nid) {
    dsm ("_stevens_mystevens_subscription_channels_get_recipient_tids_from_nid() = PASS");
  }
  else {
    dsm ("_stevens_mystevens_subscription_channels_get_recipient_tids_from_nid() = FAIL");
  }

  // Check for _stevens_mystevens_subscription_channels_get_recipient_uids_from_tids().
  $expected_recipient_uids_from_tids = array("11" => "11",
                                             "16" => "16",
                                             "31" => "31",
                                            );
  $recipient_uids_from_tids = _stevens_mystevens_subscription_channels_mailer_get_recipient_uids_from_tids($recipient_tids_from_nid);
  dsm("Expected UIDs from _stevens_mystevens_subscription_channels_get_recipient_uids_from_tids() call: ");
  dsm($expected_recipient_uids_from_tids);
  dsm("Returned UIDs from _stevens_mystevens_subscription_channels_get_recipient_uids_from_tids() call: ");
  dsm($recipient_uids_from_tids);

  if($recipient_uids_from_tids == $expected_recipient_uids_from_tids) {
    dsm ("_stevens_mystevens_subscription_channels_get_recipient_uids_from_tids() = PASS");
  }
  else {
    dsm ("_stevens_mystevens_subscription_channels_get_recipient_uids_from_tids() = FAIL");
  }

  // TODO:  Remove temporary test taxonomy
  // TODO:  Remove tempoaray test node
  // TODO:  Remove temporary test user
  // TODO:  Remove test node to terms in test vocabular via term reference on node associations
  // TODO:  Remove test user to test node via term reference on profile associates

  drupal_goto("/");
}
