<?php
?>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      img { max-width: 100%; }

      @media only screen and (max-width: 600px) {
        .wrapper { width: 95% !important; }
        .logo { width: auto; display: block !important; margin-right: 0 !important; }
        .label { width: auto; display: block !important; }
        .portrait { width: 125px !important; display: block !important; }
        .info {width: auto !important; display: block !important; }
        .description {margin-right:0 !important; margin-left: 0 !important; }
      }
      @media only screen and (max-width: 400px) {
        .portrait { width: 75px !important; display: block !important; }
      }
    </style>
  </head>
  <body>
  <!--- HEADER and LOGO------------------------>
  <table bgcolor="#871f2e"
         style="width: 100%;
                margin: 0;
                border: none">
    <tbody>
      <tr>
        <td style="display: block !important;
                   clear: both !important;
                   margin-top: 0;
                   margin-right: auto;
                   margin-bottom: 0;
                   margin-left: auto;">
          <table align="center"
                 class="wrapper"
                 style="max-width: 600px;
                        margin-top: 0;
                        margin-right: auto;
                        margin-bottom: 0;
                        margin-left: auto;">
            <tbody>
              <tr>
                <td>
                  <center>
                    <table style="margin-top: 8px;
                                  margin-right: 0;
                                  margin-bottom: 8px;
                                  margin-left: 0;
                                  width: 100%">
                      <tbody>
                        <tr>
                          <td class="logo" align="center" style="vertical-align: middle" valign="middle">
                            <img src="data:image/gif;base64,<?php echo $content['logo_base64']; // @todo Switch to embed when we have mimemail module. ?>" alt="Stevens Institute of Technology"
                              style="margin-right:0.75em;" />
                          </td>
                          <td class="label" align="center" style="margin: 0;vertical-align: middle;" valign="middle">
                          <?php if ($content['field_seminar_series_name']['#markup'] != "" && $content['field_seminar_series_name']['#markup'] != NULL) : ?>
                            <h2 style="font-size:1.4em; color:#fff;font-family:arial,sans-serif;margin-top:1em;"><?php echo $content['field_seminar_series_name']['#markup']; ?></h2>
                          <?php endif; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </center>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>

  <table bgcolor="#ffffff"
         style="width: 100%;
                margin: 0;
                border: none">
    <tr>
      <td style="display: block !important;
                 clear: both !important;
                 margin-top: 0;
                 margin-right: auto;
                 margin-bottom: 0;
                 margin-left: auto;">
        <table align="center"
               class="wrapper"
               style="max-width: 600px;
               margin-top: 0;
               margin-right: auto;
               margin-bottom: 0;
               margin-left: auto;">
          <tr>
            <td>
              <center>
                <table style="margin-top: 8px;
                              margin-right: 0;
                              margin-bottom: 15px;
                              margin-left: 0;
                              width: 100%">
                  <tr>
                    <td>
                      <h1 style="margin-top:1em;font-size:1.4em;font-family:arial,sans-serif;color:#a32638;">
                        <a href="<?php echo $content['node_url']; ?>" style="text-decoration:none;color:#a32638">
                          <?php echo $content['title']; ?>
                        </a>
                      </h1>
                      <?php if ($content['field_subtitle']['#markup'] != "" && $content['field_subtitle']['#markup'] != NULL) : ?>
                       <h2 style="margin-top:0.75em;margin-bottom:1.5em;font-size:1.3em; font-weight:normal; font-family:arial,sans-serif;color:#000;">
                        <?php echo $content['field_subtitle']['#markup']; ?>
                       </h2>
                     <?php endif; ?>

                     <?php if ($content['field_seminar_speaker_name']['#markup'] != "" && $content['field_seminar_speaker_name']['#markup'] != NULL) : ?>
                       <p style="font-size:1.25em;font-family:arial,sans-serif;color:#333;font-weight:bold;margin-bottom:0.5em;">By: <?php echo $content['field_seminar_speaker_name']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_seminar_speaker_credential']['#markup'] != "" && $content['field_seminar_speaker_credential']['#markup'] != NULL) : ?>
                       <p style="font-size:1.2em;font-family:arial,sans-serif;color:#333;margin-bottom:1.25em; margin-top:0.5em;"><?php echo $content['field_seminar_speaker_credential']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_seminar_abstract']['#markup'] != "" && $content['field_seminar_abstract']['#markup'] != NULL) : ?>
                       <h3 style="margin-top:1.75em;margin-bottom:0;font-family:arial,sans-serif; font-size:1.2em;color:#a32638;">Abstract</h3>
                       <p style="font-family:arial,sans-serif;color:#333;line-height:1.35em;margin-top:0.75em;"><?php echo $content['field_seminar_abstract']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_seminar_speaker_biography']['#markup'] != "" && $content['field_seminar_speaker_biography']['#markup'] != NULL) : ?>
                       <h3 style="margin-top:1.75em; margin-bottom:0; font-family:arial,sans-serif; font-size:1.2em; color:#a32638;">Biography</h3>
                       <p style="font-family:arial,sans-serif;color:#333;line-height:1.35em;"><?php echo $content['field_seminar_speaker_biography']['#markup']; ?></p>
                     <?php endif; ?>

                     <h3 style="font-size:1.25em;font-family:arial,sans-serif;color:#a32638;margin-top:1.75em;margin-bottom:1.25em;">Event Details</h3>

                     <?php if ($content['field_oa_date']['#markup'] != "" && $content['field_oa_date']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">Event Date</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_oa_date']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_text_location']['#markup'] != "" && $content['field_text_location']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">Location</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_text_location']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_attendance']['#markup'] != "" && $content['field_attendance']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">Attendance</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_attendance']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_rsvp']['#markup'] != "" && $content['field_rsvp']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">RSVP</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_rsvp']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_contact']['#markup'] != "" && $content['field_contact']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">Contact</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_contact']['#markup']; ?></p>
                     <?php endif; ?>

                     <?php if ($content['field_sponsor']['#markup'] != "" && $content['field_sponsor']['#markup'] != NULL) : ?>
                       <h4 style="font-size:1.0em;font-family:arial,sans-serif;color:#333;margin-bottom:0;margin-top:1.25em;text-transform:uppercase;">Sponsor</h4>
                       <p style="font-family:arial,sans-serif;color:#333;margin-top:0.75em;"><?php echo $content['field_sponsor']['#markup']; ?></p>
                     <?php endif; ?>
                    </td>
                  </tr>
                </table>
              </center>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <p>
    <a href="<?php echo $content['manage_url']; ?>">Manage myStevens Subscriptions</a>
  </p>
  </body>
</html>
