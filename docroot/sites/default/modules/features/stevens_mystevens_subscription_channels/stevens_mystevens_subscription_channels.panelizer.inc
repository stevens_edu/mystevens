<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function stevens_mystevens_subscription_channels_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'taxonomy_term:subscription_channels:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'subscription_channels';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_bryant';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '75fb0205-54c1-4404-8271-cb40b08efdc5';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e8ea4cac-337f-4405-be65-8b8eff682c02';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_view';
  $pane->subtype = 'taxonomy_term';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e8ea4cac-337f-4405-be65-8b8eff682c02';
  $display->content['new-e8ea4cac-337f-4405-be65-8b8eff682c02'] = $pane;
  $display->panels['contentmain'][0] = 'new-e8ea4cac-337f-4405-be65-8b8eff682c02';
  $pane = new stdClass();
  $pane->pid = 'new-d04c4c9b-4d5e-48b3-b5f1-fddb96602d97';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'stevens_mystevens_subscription_channels-channel_subunsub';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'd04c4c9b-4d5e-48b3-b5f1-fddb96602d97';
  $display->content['new-d04c4c9b-4d5e-48b3-b5f1-fddb96602d97'] = $pane;
  $display->panels['contentmain'][1] = 'new-d04c4c9b-4d5e-48b3-b5f1-fddb96602d97';
  $pane = new stdClass();
  $pane->pid = 'new-6a871a9a-f6f9-4bea-8e87-2c21189fd050';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'mystevens_events-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'items_per_page' => '50',
    'exposed' => array(
      'flagged' => '0',
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'table',
    'header_type' => 'titles',
    'view_mode' => 'teaser',
    'widget_title' => '',
    'context' => array(
      0 => 'panelizer',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '6a871a9a-f6f9-4bea-8e87-2c21189fd050';
  $display->content['new-6a871a9a-f6f9-4bea-8e87-2c21189fd050'] = $pane;
  $display->panels['contentmain'][2] = 'new-6a871a9a-f6f9-4bea-8e87-2c21189fd050';
  $pane = new stdClass();
  $pane->pid = 'new-9adcd53a-15e8-417f-8407-eca48ac43fe6';
  $pane->panel = 'sidebar';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'vuuid:2bd767a6-0a9e-483a-adf4-50e5fe8ff362';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'style' => 'stevens_ctp',
    'settings' => array(
      'bg_color' => 'gray-bg',
      'header_size' => 'large-header',
      'is_menu' => 0,
    ),
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9adcd53a-15e8-417f-8407-eca48ac43fe6';
  $display->content['new-9adcd53a-15e8-417f-8407-eca48ac43fe6'] = $pane;
  $display->panels['sidebar'][0] = 'new-9adcd53a-15e8-417f-8407-eca48ac43fe6';
  $pane = new stdClass();
  $pane->pid = 'new-1f2a6e95-9f44-4ff9-bfbf-77f3b46504f0';
  $pane->panel = 'sidebar';
  $pane->type = 'menu_tree';
  $pane->subtype = 'menu-channels';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'parent' => 'menu-channels:0',
    'title_link' => 0,
    'admin_title' => '',
    'level' => '1',
    'follow' => 0,
    'depth' => 0,
    'depth_relative' => 0,
    'expanded' => 0,
    'sort' => 0,
    'menu_name' => 'menu-channels',
    'parent_mlid' => '0',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '1f2a6e95-9f44-4ff9-bfbf-77f3b46504f0';
  $display->content['new-1f2a6e95-9f44-4ff9-bfbf-77f3b46504f0'] = $pane;
  $display->panels['sidebar'][1] = 'new-1f2a6e95-9f44-4ff9-bfbf-77f3b46504f0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:subscription_channels:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'taxonomy_term:subscription_channels:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'subscription_channels';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1fbcb261-bd30-4da1-a0ab-056a181f4939';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:subscription_channels:default:default'] = $panelizer;

  return $export;
}
