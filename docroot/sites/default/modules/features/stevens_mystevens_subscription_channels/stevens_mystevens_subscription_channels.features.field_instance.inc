<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function stevens_mystevens_subscription_channels_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-seminar-field_subscription_channels'.
  $field_instances['node-seminar-field_subscription_channels'] = array(
    'bundle' => 'seminar',
    'comment_alter' => 0,
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'By selecting a particular channel for this content, you will have the option to send an email notification to subscribers of that channel (and also to subscribers of parent channels, automatically).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 22,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'pane' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_subscription_channels',
    'label' => 'Channel(s)',
    'options_limit' => FALSE,
    'options_limit_empty_behaviour' => 0,
    'options_limit_fields' => array(),
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'term_reference_tree',
      'settings' => array(
        'cascading_selection' => 0,
        'disable_children' => 0,
        'disable_collapsing' => 1,
        'disable_parents' => 1,
        'filter_view' => '',
        'leaves_only' => 0,
        'max_depth' => '',
        'parent_term_id' => '',
        'select_parents' => 1,
        'start_minimized' => 0,
        'token_display' => '',
        'track_list' => 0,
      ),
      'type' => 'term_reference_tree',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'user-user-field_subscription_channels'.
  $field_instances['user-user-field_subscription_channels'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'By subscribing to a particular channel, you will receive email notifications for content associated with that channel (child channels, too).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 7,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_subscription_channels',
    'label' => 'Subscription Channels',
    'options_limit' => FALSE,
    'options_limit_empty_behaviour' => 0,
    'options_limit_fields' => array(),
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'term_reference_tree',
      'settings' => array(
        'cascading_selection' => 1,
        'disable_children' => 1,
        'disable_collapsing' => 1,
        'disable_parents' => 0,
        'filter_view' => '',
        'leaves_only' => 0,
        'max_depth' => '',
        'parent_term_id' => '',
        'select_parents' => 0,
        'start_minimized' => 0,
        'token_display' => '',
        'track_list' => 0,
      ),
      'type' => 'term_reference_tree',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('By selecting a particular channel for this content, you will have the option to send an email notification to subscribers of that channel (and also to subscribers of parent channels, automatically).');
  t('By subscribing to a particular channel, you will receive email notifications for content associated with that channel (child channels, too).');
  t('Channel(s)');
  t('Subscription Channels');

  return $field_instances;
}
