<?php
/**
 * @file
 * stevens_mystevens_subscription_channels.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function stevens_mystevens_subscription_channels_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_subscription_channels'.
  $field_bases['field_subscription_channels'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_subscription_channels',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'subscription_channels',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
