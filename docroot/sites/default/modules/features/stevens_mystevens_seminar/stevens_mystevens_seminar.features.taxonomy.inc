<?php
/**
 * @file
 * stevens_mystevens_seminar.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_mystevens_seminar_taxonomy_default_vocabularies() {
  return array(
    'seminar_series' => array(
      'name' => 'Seminar Series',
      'machine_name' => 'seminar_series',
      'description' => 'For selecting a series on Seminar content',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
