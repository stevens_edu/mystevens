<?php
/**
 * @file
 * stevens_mystevens_seminar.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function stevens_mystevens_seminar_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_oa_access|node|seminar|form';
  $field_group->group_name = 'group_oa_access';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'seminar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Access',
    'weight' => '19',
    'children' => array(
      0 => 'oa_other_spaces_ref',
      1 => 'oa_section_ref',
      2 => 'og_group_ref',
      3 => 'og_vocabulary',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Access',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_oa_access|node|seminar|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_oa_event_location|node|seminar|form';
  $field_group->group_name = 'group_oa_event_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'seminar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '21',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_oa_event_location|node|seminar|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Access');
  t('Location');

  return $field_groups;
}
