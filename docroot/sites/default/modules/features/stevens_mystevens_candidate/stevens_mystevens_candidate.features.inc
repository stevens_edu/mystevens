<?php
/**
 * @file
 * stevens_mystevens_candidate.features.inc
 */

/**
 * Implements hook_default_command_button().
 */
function stevens_mystevens_candidate_default_command_button() {
  $items = array();
  $items['candidate'] = entity_import('command_button', '{
    "bundle" : "node_add",
    "name" : "candidate",
    "title" : "Create Candidate",
    "language" : "und",
    "field_command_link" : { "und" : [
        {
          "url" : "node\\/add\\/candidate",
          "title" : "Create Candidate",
          "attributes" : []
        }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_mystevens_candidate_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_mystevens_candidate_node_info() {
  $items = array(
    'candidate' => array(
      'name' => t('Candidate'),
      'base' => 'node_content',
      'description' => t('An interview candidate.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
