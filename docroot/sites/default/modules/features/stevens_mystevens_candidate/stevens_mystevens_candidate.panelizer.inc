<?php
/**
 * @file
 * stevens_mystevens_candidate.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function stevens_mystevens_candidate_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:candidate:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'candidate';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_bryant';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '2674d998-5966-4b5e-b8c9-3e287e014cb1';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b10e579e-0860-4f35-9592-862fffe51345';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_photo';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'panopoly_image_square',
      'image_link' => '',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'center',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b10e579e-0860-4f35-9592-862fffe51345';
  $display->content['new-b10e579e-0860-4f35-9592-862fffe51345'] = $pane;
  $display->panels['contentmain'][0] = 'new-b10e579e-0860-4f35-9592-862fffe51345';
  $pane = new stdClass();
  $pane->pid = 'new-74a4ecc3-242a-4c6f-9b74-cfe8a4186b0c';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '74a4ecc3-242a-4c6f-9b74-cfe8a4186b0c';
  $display->content['new-74a4ecc3-242a-4c6f-9b74-cfe8a4186b0c'] = $pane;
  $display->panels['contentmain'][1] = 'new-74a4ecc3-242a-4c6f-9b74-cfe8a4186b0c';
  $pane = new stdClass();
  $pane->pid = 'new-59e66fae-ad59-4a74-993f-83dad6580a3b';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_curriculum_vitae';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'file_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(
      'file_view_mode' => 'default',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Curriculum Vitae',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '59e66fae-ad59-4a74-993f-83dad6580a3b';
  $display->content['new-59e66fae-ad59-4a74-993f-83dad6580a3b'] = $pane;
  $display->panels['contentmain'][2] = 'new-59e66fae-ad59-4a74-993f-83dad6580a3b';
  $pane = new stdClass();
  $pane->pid = 'new-80d5ba0f-f1dc-4d8f-b841-18b80565c902';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_schedule';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'file_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Schedule',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '80d5ba0f-f1dc-4d8f-b841-18b80565c902';
  $display->content['new-80d5ba0f-f1dc-4d8f-b841-18b80565c902'] = $pane;
  $display->panels['contentmain'][3] = 'new-80d5ba0f-f1dc-4d8f-b841-18b80565c902';
  $pane = new stdClass();
  $pane->pid = 'new-0c90bc67-cfec-4d0e-8304-57809432001b';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_presentation';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'file_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Presentation',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '0c90bc67-cfec-4d0e-8304-57809432001b';
  $display->content['new-0c90bc67-cfec-4d0e-8304-57809432001b'] = $pane;
  $display->panels['contentmain'][4] = 'new-0c90bc67-cfec-4d0e-8304-57809432001b';
  $pane = new stdClass();
  $pane->pid = 'new-d5aeeccd-19c4-4f7c-9776-ef794c39f9ac';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_present_link';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'link_url',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Link to Presentation',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'd5aeeccd-19c4-4f7c-9776-ef794c39f9ac';
  $display->content['new-d5aeeccd-19c4-4f7c-9776-ef794c39f9ac'] = $pane;
  $display->panels['contentmain'][5] = 'new-d5aeeccd-19c4-4f7c-9776-ef794c39f9ac';
  $pane = new stdClass();
  $pane->pid = 'new-339f88ef-ec44-4952-909a-3d57ca2f9083';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_other_files';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'file_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Other Files',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '339f88ef-ec44-4952-909a-3d57ca2f9083';
  $display->content['new-339f88ef-ec44-4952-909a-3d57ca2f9083'] = $pane;
  $display->panels['contentmain'][6] = 'new-339f88ef-ec44-4952-909a-3d57ca2f9083';
  $pane = new stdClass();
  $pane->pid = 'new-5bde837a-6fab-412d-a59c-d822a5b42d18';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_video_link';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'link_url',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Link to Interview Video',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = '5bde837a-6fab-412d-a59c-d822a5b42d18';
  $display->content['new-5bde837a-6fab-412d-a59c-d822a5b42d18'] = $pane;
  $display->panels['contentmain'][7] = 'new-5bde837a-6fab-412d-a59c-d822a5b42d18';
  $pane = new stdClass();
  $pane->pid = 'new-d0b690a5-77c7-42e3-892b-38effe43f854';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_other_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'link_plain',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Other Links',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = 'd0b690a5-77c7-42e3-892b-38effe43f854';
  $display->content['new-d0b690a5-77c7-42e3-892b-38effe43f854'] = $pane;
  $display->panels['contentmain'][8] = 'new-d0b690a5-77c7-42e3-892b-38effe43f854';
  $pane = new stdClass();
  $pane->pid = 'new-578e61ce-5376-400c-abc7-92b1b6fceeb0';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_candidate_feedback_survey';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'link_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 1,
    'override_title_text' => 'Feedback Survey',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 9;
  $pane->locks = array();
  $pane->uuid = '578e61ce-5376-400c-abc7-92b1b6fceeb0';
  $display->content['new-578e61ce-5376-400c-abc7-92b1b6fceeb0'] = $pane;
  $display->panels['contentmain'][9] = 'new-578e61ce-5376-400c-abc7-92b1b6fceeb0';
  $pane = new stdClass();
  $pane->pid = 'new-ecdd556a-898e-4243-a2bf-1a3b97af65c7';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'page_manager',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 10;
  $pane->locks = array();
  $pane->uuid = 'ecdd556a-898e-4243-a2bf-1a3b97af65c7';
  $display->content['new-ecdd556a-898e-4243-a2bf-1a3b97af65c7'] = $pane;
  $display->panels['contentmain'][10] = 'new-ecdd556a-898e-4243-a2bf-1a3b97af65c7';
  $pane = new stdClass();
  $pane->pid = 'new-1d55509b-38ce-41d0-8aa8-735c63a14ccc';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:og_group_ref';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'og_list_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 11;
  $pane->locks = array();
  $pane->uuid = '1d55509b-38ce-41d0-8aa8-735c63a14ccc';
  $display->content['new-1d55509b-38ce-41d0-8aa8-735c63a14ccc'] = $pane;
  $display->panels['contentmain'][11] = 'new-1d55509b-38ce-41d0-8aa8-735c63a14ccc';
  $pane = new stdClass();
  $pane->pid = 'new-86528ec9-ecde-4ceb-a76a-ee74bc3ef3a5';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'node:webform';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Feedback Survey',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 12;
  $pane->locks = array();
  $pane->uuid = '86528ec9-ecde-4ceb-a76a-ee74bc3ef3a5';
  $display->content['new-86528ec9-ecde-4ceb-a76a-ee74bc3ef3a5'] = $pane;
  $display->panels['contentmain'][12] = 'new-86528ec9-ecde-4ceb-a76a-ee74bc3ef3a5';
  $pane = new stdClass();
  $pane->pid = 'new-a316659e-3efd-4f92-8676-be81a60b072a';
  $pane->panel = 'sidebar';
  $pane->type = 'og_menu_single_menu';
  $pane->subtype = 'og_menu_single_menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'parent' => 0,
    'return' => 'Finish',
    'cancel' => 'Cancel',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'og_menu_single_depth' => '0',
    'og_menu_single_parent' => '0',
    'form_build_id' => 'form-Vxi_ODSaodvB3q9x3XAwbuKx9PZ7w53z0J19IC_uTtc',
    'form_token' => 'WP2cSh5yObtaTQIPGS9arNgsPKwq1JKnR-bh5KKVW20',
    'form_id' => 'og_menu_single_pane_edit_form',
    'op' => 'Finish',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a316659e-3efd-4f92-8676-be81a60b072a';
  $display->content['new-a316659e-3efd-4f92-8676-be81a60b072a'] = $pane;
  $display->panels['sidebar'][0] = 'new-a316659e-3efd-4f92-8676-be81a60b072a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:candidate:default'] = $panelizer;

  return $export;
}
