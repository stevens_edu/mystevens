<?php
/**
 * @file
 * stevens_intranet_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_intranet_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_search_api_index().
 */
function stevens_intranet_search_default_search_api_index() {
  $items = array();
  $items['acquia_search_node_index'] = entity_import('search_api_index', '{
    "name" : "Acquia Search Node Index",
    "machine_name" : "acquia_search_node_index",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "attachments_field_basic_file_file" : { "type" : "text" },
        "attachments_field_oa_media" : { "type" : "text" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:summary" : { "type" : "text" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_oa_banner_text" : { "type" : "text" },
        "field_oa_media:file" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "file" },
        "field_oa_related" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "paragraphs_item" },
        "field_oa_related:field_paragraph_text:value" : { "type" : "list\\u003Ctext\\u003E" },
        "field_oa_related:field_snippet_title_override" : { "type" : "list\\u003Ctext\\u003E" },
        "field_oa_related:snippet_body" : { "type" : "list\\u003Ctext\\u003E" },
        "field_oa_related:snippet_title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_title" : { "type" : "text" },
        "nid" : { "type" : "integer" },
        "oa_section_ref" : { "type" : "integer", "entity_type" : "node" },
        "oa_section_ref:field_oa_banner_text" : { "type" : "text" },
        "oa_section_ref:field_title" : { "type" : "text" },
        "oa_section_ref:title" : { "type" : "text" },
        "og_group_ref" : { "type" : "integer", "entity_type" : "node" },
        "og_group_ref:field_title" : { "type" : "text" },
        "og_group_ref:title" : { "type" : "text" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "search_api_panelizer_content" : { "type" : "text" },
        "search_api_panelizer_title" : { "type" : "text" },
        "search_api_url" : { "type" : "uri" },
        "search_api_viewed" : { "type" : "text" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text", "boost" : "8.0" },
        "type" : { "type" : "string" },
        "url" : { "type" : "uri" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-50",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-48", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-47", "settings" : { "fields" : [] } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "-46", "settings" : [] },
        "search_api_alter_add_viewed_entity" : {
          "status" : 1,
          "weight" : "-45",
          "settings" : { "mode" : "search_result" }
        },
        "panelizer" : { "status" : 1, "weight" : "-44", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 1, "weight" : "-43", "settings" : [] },
        "search_api_attachments_alter_settings" : {
          "status" : 1,
          "weight" : "-42",
          "settings" : {
            "excluded_extensions" : "aif art avi bmp gif ico mov oga ogv png psd ra ram rgb flv",
            "number_indexed" : "0",
            "max_file_size" : "0",
            "excluded_private" : 0
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_oa_banner_text" : true,
              "field_title" : true,
              "search_api_viewed" : true,
              "search_api_panelizer_content" : true,
              "search_api_panelizer_title" : true,
              "attachments_field_oa_media" : true,
              "attachments_field_basic_file_file" : true,
              "body:value" : true,
              "body:summary" : true,
              "field_oa_related:snippet_body" : true,
              "og_group_ref:title" : true,
              "oa_section_ref:title" : true,
              "oa_section_ref:field_oa_banner_text" : true,
              "oa_section_ref:field_title" : true,
              "field_oa_related:field_paragraph_text:value" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "field_oa_banner_text" : true,
              "search_api_viewed" : true,
              "search_api_panelizer_content" : true,
              "search_api_panelizer_title" : true,
              "body:value" : true,
              "body:summary" : true,
              "field_oa_related:snippet_body" : true,
              "oa_section_ref:field_oa_banner_text" : true,
              "field_oa_related:field_paragraph_text:value" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : { "status" : 0, "weight" : "15", "settings" : { "fields" : [] } },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_oa_banner_text" : true,
              "field_title" : true,
              "search_api_viewed" : true,
              "search_api_panelizer_content" : true,
              "search_api_panelizer_title" : true,
              "attachments_field_oa_media" : true,
              "attachments_field_basic_file_file" : true,
              "body:value" : true,
              "body:summary" : true,
              "field_oa_related:snippet_body" : true,
              "og_group_ref:title" : true,
              "oa_section_ref:title" : true,
              "oa_section_ref:field_oa_banner_text" : true,
              "oa_section_ref:field_title" : true,
              "field_oa_related:field_paragraph_text:value" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : [],
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  $items['acquia_search_user_index'] = entity_import('search_api_index', '{
    "name" : "Acquia Search User Index",
    "machine_name" : "acquia_search_user_index",
    "description" : null,
    "server" : "acquia_search",
    "item_type" : "user",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "field_user_about:summary" : { "type" : "text" },
        "field_user_display_name" : { "type" : "text", "boost" : "5.0" },
        "mail" : { "type" : "text" },
        "name" : { "type" : "text" },
        "og_user_node" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "search_api_language" : { "type" : "string" },
        "search_api_panelizer_content" : { "type" : "text" },
        "search_api_panelizer_title" : { "type" : "text" },
        "search_api_url" : { "type" : "uri" },
        "uid" : { "type" : "integer" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_role_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "roles" : [] }
        },
        "search_api_attachments_alter_settings" : {
          "status" : 1,
          "weight" : "0",
          "settings" : {
            "excluded_extensions" : "aif art avi bmp gif ico mov oga ogv png psd ra ram rgb flv",
            "number_indexed" : "0",
            "max_file_size" : "0",
            "excluded_private" : 0
          }
        },
        "panelizer" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "name" : true,
              "mail" : true,
              "field_user_display_name" : true,
              "search_api_panelizer_content" : true,
              "search_api_panelizer_title" : true,
              "field_user_about:summary" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "search_api_panelizer_content" : true,
              "field_user_about:summary" : true
            },
            "title" : 1,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : { "status" : 0, "weight" : "15", "settings" : { "fields" : [] } },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "name" : true,
              "mail" : true,
              "field_user_display_name" : true,
              "search_api_panelizer_content" : true,
              "search_api_panelizer_title" : true,
              "field_user_about:summary" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : [],
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function stevens_intranet_search_default_search_api_server() {
  $items = array();
  $items['acquia_search'] = entity_import('search_api_server', '{
    "name" : "Acquia Search",
    "machine_name" : "acquia_search",
    "description" : "Acquia Search Solr server as configured per their Search API documentation (not their Apache Solr Search Integration documentation) at https:\\/\\/docs.acquia.com\\/acquia-search\\/search-api",
    "class" : "acquia_search_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "http",
      "host" : "useast1-c1.acquia-search.com",
      "port" : "80",
      "path" : "\\/solr\\/IMQZ-54964",
      "edismax" : 0,
      "modify_acquia_connection" : 0,
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 1,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO"
    },
    "enabled" : "1"
  }');
  return $items;
}
