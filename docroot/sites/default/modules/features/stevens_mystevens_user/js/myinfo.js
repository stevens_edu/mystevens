(function ($) {
  $(document).ready(function () { 
    $('#cwid-tog').click(function() {
      $('#info-cwid').toggle();
    });
    $('#ip-tog').click(function() {
      $('#info-ip').toggle();
    });
    $('#w3groups-tog').click(function() {
      $('#info-w3groups').toggle();
    });
  });
})(jQuery);
