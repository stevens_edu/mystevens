(function ($) {
  jQuery(document).ready(function () {
    jQuery.ajax({
      type: 'POST',
      url: "https://api.stevens.edu/esp/w3groups",
      data: JSON.stringify(Drupal.settings.stevens_esp.creds),
      success: function (data) {
                 if (!jQuery.isEmptyObject(data)) {
                   jQuery('#w3groups-tog-wrap').toggle();
                   jQuery("#info-w3groups").append('<ul id="w3groups-list"></ul>');
                 }
                 jQuery.each(data, function(key, content){
                   var w3group = content.cn.substring(2);
                   var w3groupRendered = '<a href="https://www.stevens.edu/' + w3group + '/" target="_blank">' + w3group + '</a>';
                   jQuery("#w3groups-list").append('<li id="w3group-' + w3group + '" class="w3group">' + w3groupRendered + '</li>');
                   jQuery("#w3group-" + w3group).append('<ul></ul>');
                   jQuery.each(content.members, function(key, user){
                     jQuery("#w3group-" + w3group + " ul").append("<li>" + user + "</li>");
                   });
                 });
               },
       contentType: "application/json",
       dataType: 'json'
     })
  });
})(jQuery);

