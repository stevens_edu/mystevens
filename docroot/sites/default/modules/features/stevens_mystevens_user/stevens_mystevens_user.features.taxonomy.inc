<?php
/**
 * @file
 * stevens_mystevens_user.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_mystevens_user_taxonomy_default_vocabularies() {
  return array(
    'stevens_mystevens_criteria_sets' => array(
      'name' => 'MyStevens Criteria Sets',
      'machine_name' => 'stevens_mystevens_criteria_sets',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
