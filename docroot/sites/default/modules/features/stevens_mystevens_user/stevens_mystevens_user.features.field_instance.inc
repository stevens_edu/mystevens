<?php
/**
 * @file
 * stevens_mystevens_user.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function stevens_mystevens_user_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_user_criteria_met'.
  $field_instances['user-user-field_user_criteria_met'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 6,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_user_criteria_met',
    'label' => 'Criteria Met',
    'options_limit' => FALSE,
    'options_limit_empty_behaviour' => 0,
    'options_limit_fields' => array(),
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => 0,
      ),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Criteria Met');

  return $field_instances;
}
