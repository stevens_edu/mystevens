<?php
/**
 * @file
 * stevens_mystevens_directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_mystevens_directory_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stevens_mystevens_directory_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function stevens_mystevens_directory_node_info() {
  $items = array(
    'office' => array(
      'name' => t('Office'),
      'base' => 'node_content',
      'description' => t('Content that will be displayed in the myStevens Directory'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
