<?php
/**
 * @file
 * stevens_mystevens_directory.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_mystevens_directory_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'directory';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Directory';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results found.';
  $handler->display->display_options['empty']['area']['format'] = 'panopoly_wysiwyg_text';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_office_link']['id'] = 'field_office_link';
  $handler->display->display_options['fields']['field_office_link']['table'] = 'field_data_field_office_link';
  $handler->display->display_options['fields']['field_office_link']['field'] = 'field_office_link';
  $handler->display->display_options['fields']['field_office_link']['label'] = '';
  $handler->display->display_options['fields']['field_office_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_office_link']['alter']['text'] = 'View Website';
  $handler->display->display_options['fields']['field_office_link']['alter']['path'] = '[field_office_link-url]';
  $handler->display->display_options['fields']['field_office_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_office_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_office_link']['type'] = 'link_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_office_link]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_icon']['settings'] = array(
    'image_style' => 'oa_small_thumbnail',
    'image_link' => '',
  );
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Title */
  $handler->display->display_options['arguments']['title']['id'] = 'title';
  $handler->display->display_options['arguments']['title']['table'] = 'node';
  $handler->display->display_options['arguments']['title']['field'] = 'title';
  $handler->display->display_options['arguments']['title']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['title']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['title']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['title']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['title']['glossary'] = TRUE;
  $handler->display->display_options['arguments']['title']['limit'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'office' => 'office',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Tabs';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    31 => 0,
  );
  $handler->display->display_options['filters']['type']['expose']['reduce'] = TRUE;
  $handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['group_info']['default_group'] = '1';
  $handler->display->display_options['filters']['type']['group_info']['default_group_multiple'] = array(
    1 => '1',
    2 => '2',
  );
  $handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'All',
      'operator' => 'in',
      'value' => array(
        'mystevens_item' => 'mystevens_item',
        'oa_space' => 'oa_space',
        'all' => 0,
        'announcement' => 0,
        'blog' => 0,
        'citrix_file_' => 0,
        'contact_us' => 0,
        'panopoly_page' => 0,
        'oa_discussion_post' => 0,
        'oa_wiki_page' => 0,
        'duckbills_merchant' => 0,
        'oa_event' => 0,
        'oa_group' => 0,
        'panopoly_landing_page' => 0,
        'office_hours' => 0,
        'orgsync_undergrad_student_life_c' => 0,
        'projects' => 0,
        'promotional_area' => 0,
        'oa_section' => 0,
        'oa_worktracker_task' => 0,
        'oa_team' => 0,
        'training' => 0,
        'webform' => 0,
        'what_we_do' => 0,
        'who_we_are' => 0,
        'oa_ical_importer' => 0,
      ),
    ),
    2 => array(
      'title' => 'Spaces',
      'operator' => 'in',
      'value' => array(
        'oa_space' => 'oa_space',
        'all' => 0,
        'announcement' => 0,
        'blog' => 0,
        'citrix_file_' => 0,
        'contact_us' => 0,
        'panopoly_page' => 0,
        'oa_discussion_post' => 0,
        'oa_wiki_page' => 0,
        'duckbills_merchant' => 0,
        'oa_event' => 0,
        'oa_group' => 0,
        'panopoly_landing_page' => 0,
        'mystevens_item' => 0,
        'office_hours' => 0,
        'orgsync_undergrad_student_life_c' => 0,
        'projects' => 0,
        'promotional_area' => 0,
        'oa_section' => 0,
        'oa_worktracker_task' => 0,
        'oa_team' => 0,
        'training' => 0,
        'webform' => 0,
        'what_we_do' => 0,
        'who_we_are' => 0,
        'oa_ical_importer' => 0,
      ),
    ),
    3 => array(
      'title' => 'Services',
      'operator' => 'in',
      'value' => array(
        'mystevens_item' => 'mystevens_item',
        'all' => 0,
        'announcement' => 0,
        'blog' => 0,
        'citrix_file_' => 0,
        'contact_us' => 0,
        'panopoly_page' => 0,
        'oa_discussion_post' => 0,
        'oa_wiki_page' => 0,
        'duckbills_merchant' => 0,
        'oa_event' => 0,
        'oa_group' => 0,
        'panopoly_landing_page' => 0,
        'office_hours' => 0,
        'orgsync_undergrad_student_life_c' => 0,
        'projects' => 0,
        'promotional_area' => 0,
        'oa_section' => 0,
        'oa_space' => 0,
        'oa_worktracker_task' => 0,
        'oa_team' => 0,
        'training' => 0,
        'webform' => 0,
        'what_we_do' => 0,
        'who_we_are' => 0,
        'oa_ical_importer' => 0,
      ),
    ),
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '1';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'directory';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Directory';
  $export['directory'] = $view;

  return $export;
}
