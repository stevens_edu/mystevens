<?php
/**
 * @file
 * stevens_mystevens_themes.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function stevens_mystevens_themes_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: panels_mini
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-2b828bf5-01a6-4805-a95d-6c64c67317ef|access|plugins"] = array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 11,
          1 => 16,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-2b828bf5-01a6-4805-a95d-6c64c67317ef|position"] = 0;
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-3bd6fb98-8bec-48c8-9777-cf8850617d9f|access|plugins"] = array();
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-3bd6fb98-8bec-48c8-9777-cf8850617d9f|position"] = 1;
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-5c14b4dd-32e7-4bc0-abaa-a45d8cfd8cf2"] = (object) array(
      'pid' => 'new-5c14b4dd-32e7-4bc0-abaa-a45d8cfd8cf2',
      'panel' => 'header',
      'type' => 'custom',
      'subtype' => 'custom',
      'shown' => TRUE,
      'access' => array(
        'plugins' => array(
          0 => array(
            'name' => 'theme',
            'settings' => array(
              'theme' => 'stevens_intranet',
            ),
            'not' => FALSE,
          ),
        ),
      ),
      'configuration' => array(
        'admin_title' => 'MyStevens Masthead',
        'title' => '',
        'body' => '<div class="mystevens-header">
                        <div class="mystevens-logo-cont">
                          <a href="/"><img alt="myStevens Logo" title="myStevens Logo" class="mystevens-logo-img" src="/sites/default/themes/stevens_intranet/assets/images/logo.svg" /></a>
                        </div>
                        <div class="mystevens-header-links-cont">
                            <ul>
                              <li class="header-link-cont">
                                <a class="header-link" href="http://www.stevens.edu">Stevens.edu</a>
                              </li>
                              <li class="header-link-spacer">|</li>
                              <li class="header-link-cont">
                                <a class="header-link" href="https://web.stevens.edu/peoplefinder/">People Finder</a>
                              </li>
                        </div>
                    </div>',
        'format' => 'panopoly_html_text',
        'substitute' => TRUE,
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 1,
      'locks' => array(),
      'uuid' => '5c14b4dd-32e7-4bc0-abaa-a45d8cfd8cf2',
    );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-70813a7e-2551-49da-bd7c-4acaff4cae42|access|plugins"] = array();
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-70813a7e-2551-49da-bd7c-4acaff4cae42|css|css_class"] = 'search-mobile-menu pull-left';
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-738df920-1380-4622-ba4c-21f486060818|access|plugins"] = array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 11,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-7d485ce0-ad68-4f5a-85bb-debf0565fcf6"] = (object) array(
      'pid' => 'new-7d485ce0-ad68-4f5a-85bb-debf0565fcf6',
      'panel' => 'sidebar',
      'type' => 'block',
      'subtype' => 'stevens_oa-header_user_old',
      'shown' => TRUE,
      'access' => array(
        'plugins' => array(
          0 => array(
            'name' => 'theme',
            'settings' => array(
              'theme' => 'stevens_oa_radix',
            ),
            'not' => FALSE,
          ),
        ),
      ),
      'configuration' => array(
        'override_title' => 0,
        'override_title_text' => '',
        'override_title_heading' => 'h2',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 0,
      'locks' => array(),
      'uuid' => '7d485ce0-ad68-4f5a-85bb-debf0565fcf6',
    );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-818fa3a5-376e-4e7f-8f98-77ed1a6c3252|access|plugins"] = array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 11,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-8392382b-5711-4749-96e6-48ae5a7dda23|access|plugins"] = array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 11,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|access|plugins"] = array(
    0 => array(
      'name' => 'theme',
      'settings' => array(
        'theme' => 'stevens_intranet',
      ),
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|configuration|form_build_id"] = 'form-vq9vCgHSmardn4pIcTo5kxvzQsVV8r7grR7vbo-MttI';
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|configuration|form_token"] = 'Iv59AalW-y4iBl-rqAWo8vxHH8EaR7RWNdnqBqkQzQU';
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|configuration|menu_name"] = 'og-menu-single';
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|configuration|override_title_heading"] = 'h2';
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24|position"] = 0;
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-bf0e9047-1f82-4bcf-bf9b-f97fe6748f41|access|plugins"] = array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 11,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-d02119ab-43a7-4f5b-98a8-24506b23a2d2|access|plugins"] = array(
    0 => array(
      'name' => 'theme',
      'settings' => array(
        'theme' => 'stevens_oa_radix',
      ),
      'not' => FALSE,
    ),
  );
  $overrides["panels_mini.oa_toolbar_panel.display|content|new-d02119ab-43a7-4f5b-98a8-24506b23a2d2|panel"] = 'contentmain';
  $overrides["panels_mini.oa_toolbar_panel.display|panels|contentmain"] = array(
    0 => 'new-d02119ab-43a7-4f5b-98a8-24506b23a2d2',
  );
  $overrides["panels_mini.oa_toolbar_panel.display|panels|footer|0"] = 'new-2b828bf5-01a6-4805-a95d-6c64c67317ef';
  $overrides["panels_mini.oa_toolbar_panel.display|panels|footer|1"] = 'new-3bd6fb98-8bec-48c8-9777-cf8850617d9f';
  $overrides["panels_mini.oa_toolbar_panel.display|panels|header|0"] = 'new-86bc5dc1-c1f8-4b43-a40c-7e7a36198b24';
  $overrides["panels_mini.oa_toolbar_panel.display|panels|header|1"] = 'new-5c14b4dd-32e7-4bc0-abaa-a45d8cfd8cf2';
  $overrides["panels_mini.oa_toolbar_panel.display|panels|sidebar"] = array(
    0 => 'new-7d485ce0-ad68-4f5a-85bb-debf0565fcf6',
  );

 return $overrides;
}
