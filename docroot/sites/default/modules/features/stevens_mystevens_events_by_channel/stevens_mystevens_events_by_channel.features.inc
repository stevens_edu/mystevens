<?php
/**
 * @file
 * stevens_mystevens_events_by_channel.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stevens_mystevens_events_by_channel_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
