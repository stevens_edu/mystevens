<?php

/**
 * @file
 * Key shim for HTML Body of notification emails from Atrium.
 *
 * Because the oa-messages-body--[message_type].tpl.php template suggestion
 * system is not operable at the moment, this file decides which template to
 * use based on $key.
 */

if (substr($key, -16) == '-mystevens-event') {
  include 'mystevens_event.tpl.php.inc';
}
else {
  include 'original.tpl.php.inc';
}
