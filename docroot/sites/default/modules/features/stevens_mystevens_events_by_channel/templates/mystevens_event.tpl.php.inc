<?php

/**
 * @file
 * Default theme template for HTML Body of notification emails from Atrium.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to oa-messages-body--[message_type].tpl.php to override it for a
 * specific message type.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier (type)
 * - $message: The full message object
 * - $timestamp: timestamp of message
 *
 * @see template_preprocess_mimemail_message()
 */

/*
  NOTE: We use some inline styles here because GMail strips css in the
  <style> tag of the email.  So really important css needs to be inline.
*/
?>

<?php
  // The $body variable contains a node ID. Completely sanitize it.
  $nid = strip_tags($body);
  $nid = (int) $nid;
  // If the node exists, render and print it.
  if ($node = node_load($nid)) {
    $build = node_view($node);
    // Hide contextual links and headshot. Everything else in the node template is rendered.
    $build['links']['#access'] = FALSE;
    $build['field_seminar_speaker_headshot']['#access'] = FALSE;

    $rendered_node = drupal_render($build);
    $node_title = $build['#node']->title;
    $space_title = $build['og_group_ref'][0]['#title'];
    $space_href = $build['og_group_ref'][0]['#href'];
  }
  else {
    $rendered_node = "<p>Node $nid not found.</p>";
  }
?>

<?php if (!empty($message['separator'])): ?>
  <?php print $message['separator']; ?>
<?php endif; ?>
<div class="mail-table">
  <div class="heading" style="padding:10px;border-bottom:1px solid #EEE;">
    <h3 style="margin:0;"><?php print l($node_title, "node/$nid"); ?></h3>
  </div>
  <div class="body" style="padding:10px;">
    <?php print $rendered_node; ?>
  </div>
  <div class="footer" style="padding:10px; border-top:1px solid #EEE;">
    You are following <?php print l($space_title, $space_href); ?>
  </div>
</div>
