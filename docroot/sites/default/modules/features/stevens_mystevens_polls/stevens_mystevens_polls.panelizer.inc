<?php
/**
 * @file
 * stevens_mystevens_polls.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function stevens_mystevens_polls_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:poll:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'poll';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe_node_access';
  $panelizer->contexts = array();
  $panelizer->relationships = array(
    0 => array(
      'identifier' => 'Poll Author',
      'keyword' => 'user',
      'name' => 'entity_from_schema:uid-node-user',
      'context' => 'panelizer',
      'id' => 1,
    ),
    1 => array(
      'identifier' => 'Space',
      'keyword' => 'space',
      'name' => 'entity_from_field:og_group_ref-node-node',
      'delta' => 0,
      'context' => 'panelizer',
      'id' => 1,
    ),
    2 => array(
      'identifier' => 'Section',
      'keyword' => 'space',
      'name' => 'entity_from_field:oa_section_ref-node-node',
      'delta' => 0,
      'context' => 'panelizer',
      'id' => 1,
    ),
  );
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_bryant';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'c3d037fc-11b3-4fb3-a8cc-eefc61a21e37';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-be1dedea-5da7-413f-8485-ed1352733586';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'node:poll_view_voting';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'be1dedea-5da7-413f-8485-ed1352733586';
  $display->content['new-be1dedea-5da7-413f-8485-ed1352733586'] = $pane;
  $display->panels['contentmain'][0] = 'new-be1dedea-5da7-413f-8485-ed1352733586';
  $pane = new stdClass();
  $pane->pid = 'new-36d7f63e-9acb-442a-b7b4-9cdd1ea9564f';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'node:poll_view_results';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '36d7f63e-9acb-442a-b7b4-9cdd1ea9564f';
  $display->content['new-36d7f63e-9acb-442a-b7b4-9cdd1ea9564f'] = $pane;
  $display->panels['contentmain'][1] = 'new-36d7f63e-9acb-442a-b7b4-9cdd1ea9564f';
  $pane = new stdClass();
  $pane->pid = 'new-97bdd038-b3f8-4f57-9581-5387b385d68a';
  $pane->panel = 'sidebar';
  $pane->type = 'og_menu_single_menu';
  $pane->subtype = 'og_menu_single_menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'parent' => 0,
    'return' => 'Finish',
    'cancel' => 'Cancel',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'og_menu_single_depth' => '0',
    'og_menu_single_parent' => '0',
    'form_build_id' => 'form-qb3aT9GT61zyKb08RAYO1li426cLlVhlq0ucFl1CImM',
    'form_token' => 'nSjcWrp92m6NCNNHH-4TSxsEPF7wcpbwz2s6nOje0vM',
    'form_id' => 'og_menu_single_pane_edit_form',
    'op' => 'Finish',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '97bdd038-b3f8-4f57-9581-5387b385d68a';
  $display->content['new-97bdd038-b3f8-4f57-9581-5387b385d68a'] = $pane;
  $display->panels['sidebar'][0] = 'new-97bdd038-b3f8-4f57-9581-5387b385d68a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:poll:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:poll:default:featured';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'poll';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'featured';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '9a1f07ba-4b72-4859-ac0c-e1e67017f3c0';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-08b34baf-dddd-4108-993e-c4c816b8c790';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:og_vocabulary';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '08b34baf-dddd-4108-993e-c4c816b8c790';
  $display->content['new-08b34baf-dddd-4108-993e-c4c816b8c790'] = $pane;
  $display->panels['center'][0] = 'new-08b34baf-dddd-4108-993e-c4c816b8c790';
  $pane = new stdClass();
  $pane->pid = 'new-f375ee28-7da5-4a65-adfb-859b3ea17420';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:oa_other_spaces_ref';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'f375ee28-7da5-4a65-adfb-859b3ea17420';
  $display->content['new-f375ee28-7da5-4a65-adfb-859b3ea17420'] = $pane;
  $display->panels['center'][1] = 'new-f375ee28-7da5-4a65-adfb-859b3ea17420';
  $pane = new stdClass();
  $pane->pid = 'new-cd05296c-fcb1-4358-9807-09c4e24a4c1e';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:oa_section_ref';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'cd05296c-fcb1-4358-9807-09c4e24a4c1e';
  $display->content['new-cd05296c-fcb1-4358-9807-09c4e24a4c1e'] = $pane;
  $display->panels['center'][2] = 'new-cd05296c-fcb1-4358-9807-09c4e24a4c1e';
  $pane = new stdClass();
  $pane->pid = 'new-fb5b51f0-7b4f-430b-bfdd-bd9378b8fc2b';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:og_group_ref';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fb5b51f0-7b4f-430b-bfdd-bd9378b8fc2b';
  $display->content['new-fb5b51f0-7b4f-430b-bfdd-bd9378b8fc2b'] = $pane;
  $display->panels['center'][3] = 'new-fb5b51f0-7b4f-430b-bfdd-bd9378b8fc2b';
  $pane = new stdClass();
  $pane->pid = 'new-9826af25-dde4-4313-890a-20610db64db4';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'featured',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9826af25-dde4-4313-890a-20610db64db4';
  $display->content['new-9826af25-dde4-4313-890a-20610db64db4'] = $pane;
  $display->panels['center'][4] = 'new-9826af25-dde4-4313-890a-20610db64db4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-9826af25-dde4-4313-890a-20610db64db4';
  $panelizer->display = $display;
  $export['node:poll:default:featured'] = $panelizer;

  return $export;
}
