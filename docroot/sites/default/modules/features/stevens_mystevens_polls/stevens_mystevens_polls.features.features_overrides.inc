<?php
/**
 * @file
 * stevens_mystevens_polls.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function stevens_mystevens_polls_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base
  $overrides["field_base.field_oa_related_content.settings|handler_settings|target_bundles|poll"] = 'poll';

  // Exported overrides for: node
  $overrides["node.poll.description"] = 'A <em>poll</em> is a question with a set of possible responses. A <em>poll</em>, once created, automatically provides a simple running count of the number of votes received for each response.  Modified by Stevens to use Open Atrium ACLs.';
  $overrides["node.poll.has_title"] = 1;
  $overrides["node.poll.help"] = '';

 return $overrides;
}
