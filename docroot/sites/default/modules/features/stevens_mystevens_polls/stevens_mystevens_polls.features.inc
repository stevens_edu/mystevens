<?php
/**
 * @file
 * stevens_mystevens_polls.features.inc
 */

/**
 * Implements hook_default_command_button().
 */
function stevens_mystevens_polls_default_command_button() {
  $items = array();
  $items['advpoll'] = entity_import('command_button', '{
    "bundle" : "node_add",
    "name" : "advpoll",
    "title" : "Create Advanced Poll",
    "language" : "und",
    "field_command_link" : { "und" : [
        {
          "url" : "node\\/add\\/advpoll",
          "title" : "Create Advanced Poll",
          "attributes" : []
        }
      ]
    }
  }');
  $items['poll'] = entity_import('command_button', '{
    "bundle" : "node_add",
    "name" : "poll",
    "title" : "Create Poll",
    "language" : "und",
    "field_command_link" : { "und" : [
        { "url" : "node\\/add\\/poll", "title" : "Create Poll", "attributes" : [] }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_mystevens_polls_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stevens_mystevens_polls_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_field_default_field_bases_alter().
 */
function stevens_mystevens_polls_field_default_field_bases_alter(&$data) {
  if (isset($data['field_oa_related_content'])) {
    $data['field_oa_related_content']['settings']['handler_settings']['target_bundles']['poll'] = 'poll'; /* WAS: '' */
  }
}

/**
 * Implements hook_node_info_alter().
 */
function stevens_mystevens_polls_node_info_alter(&$data) {
  if (isset($data['poll'])) {
    $data['poll']['description'] = 'A <em>poll</em> is a question with a set of possible responses. A <em>poll</em>, once created, automatically provides a simple running count of the number of votes received for each response.  Modified by Stevens to use Open Atrium ACLs.'; /* WAS: 'A <em>poll</em> is a question with a set of possible responses. A <em>poll</em>, once created, automatically provides a simple running count of the number of votes received for each response.' */
    $data['poll']['has_title'] = 1; /* WAS: '' */
    $data['poll']['help'] = ''; /* WAS: '' */
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_mystevens_polls_node_info() {
  $items = array(
    'advpoll' => array(
      'name' => t('Advanced Poll'),
      'base' => 'node_content',
      'description' => t('Advanced Poll adds additional poll functionality, cookie voting, write-ins and voting modes.'),
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
