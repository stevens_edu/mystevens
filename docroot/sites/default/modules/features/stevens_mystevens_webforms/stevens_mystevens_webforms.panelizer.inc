<?php
/**
 * @file
 * stevens_mystevens_webforms.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function stevens_mystevens_webforms_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oa_section:webform_section';
  $panelizer->title = 'Webform Section';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oa_section';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe_node_access';
  $panelizer->contexts = array();
  $panelizer->relationships = array(
    0 => array(
      'identifier' => 'Space',
      'keyword' => 'space',
      'name' => 'entity_from_field:og_group_ref-node-node',
      'delta' => 0,
      'context' => 'panelizer',
      'id' => 1,
    ),
  );
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_bryant_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
    'sidebar' => array(
      'style' => 'oa_styles_oa_pane',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6b061dde-e76e-4840-87d6-3c2d260e5bd0';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1f764f22-e7d4-4e51-ae8f-15eb0505f91d';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1f764f22-e7d4-4e51-ae8f-15eb0505f91d';
  $display->content['new-1f764f22-e7d4-4e51-ae8f-15eb0505f91d'] = $pane;
  $display->panels['contentmain'][0] = 'new-1f764f22-e7d4-4e51-ae8f-15eb0505f91d';
  $pane = new stdClass();
  $pane->pid = 'new-cb1dd040-482d-4e4d-a629-86a18caf7ff3';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'webform_view-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'exposed' => array(
      'og_group_ref_target_id' => 'CURRENT',
      'og_group_ref_target_id_mine' => 0,
      'og_subspaces_view_all' => 0,
      'og_subspaces_view_parent' => 0,
    ),
    'show_exposed_form' => 0,
    'fields_override' => array(
      'nid' => 0,
      'title' => 1,
      'body' => 1,
      'field_img' => 1,
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'titles',
    'view_mode' => 'full',
    'widget_title' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'cb1dd040-482d-4e4d-a629-86a18caf7ff3';
  $display->content['new-cb1dd040-482d-4e4d-a629-86a18caf7ff3'] = $pane;
  $display->panels['contentmain'][1] = 'new-cb1dd040-482d-4e4d-a629-86a18caf7ff3';
  $pane = new stdClass();
  $pane->pid = 'new-8a102498-9433-4daa-89e8-02137abfcfc0';
  $pane->panel = 'sidebar';
  $pane->type = 'oa_widgets_content_visibility';
  $pane->subtype = 'oa_widgets_content_visibility';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'return' => 'Finish',
    'cancel' => 'Cancel',
    'override_title' => 0,
    'override_title_text' => '',
    'form_build_id' => 'form-AHJUfpYPcZ4fsSHUHIFy0Adu0CXy4e79Kcslz2yLodI',
    'form_token' => 'BzH8uxEwTjsQGuqkcXBIlcjCxMTFGGufXWz-7ckhC2M',
    'form_id' => 'oa_widgets_content_visibility_edit_form',
    'op' => 'Finish',
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8a102498-9433-4daa-89e8-02137abfcfc0';
  $display->content['new-8a102498-9433-4daa-89e8-02137abfcfc0'] = $pane;
  $display->panels['sidebar'][0] = 'new-8a102498-9433-4daa-89e8-02137abfcfc0';
  $pane = new stdClass();
  $pane->pid = 'new-e46eaca6-3e81-4a12-acd9-72122f1f183f';
  $pane->panel = 'sidebar';
  $pane->type = 'og_menu_single_menu';
  $pane->subtype = 'og_menu_single_menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'parent' => 0,
    'return' => 'Finish',
    'cancel' => 'Cancel',
    'override_title' => 0,
    'override_title_text' => '',
    'og_menu_single_depth' => '0',
    'og_menu_single_parent' => 'auto',
    'form_build_id' => 'form-4K3ewrxBpQ6gLpQ17PMXLK44cCFWxCLoQS-Yp1Hks_E',
    'form_token' => '1Bb8t5EXHaEf_Y5XvtojdqAxSDMGwVGyanDS7B9BUEs',
    'form_id' => 'og_menu_single_pane_edit_form',
    'op' => 'Finish',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'e46eaca6-3e81-4a12-acd9-72122f1f183f';
  $display->content['new-e46eaca6-3e81-4a12-acd9-72122f1f183f'] = $pane;
  $display->panels['sidebar'][1] = 'new-e46eaca6-3e81-4a12-acd9-72122f1f183f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oa_section:webform_section'] = $panelizer;

  return $export;
}
