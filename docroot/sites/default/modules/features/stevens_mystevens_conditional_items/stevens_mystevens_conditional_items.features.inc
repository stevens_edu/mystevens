<?php

/**
 * @file
 * stevens_mystevens_conditional_items.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_mystevens_conditional_items_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stevens_mystevens_conditional_items_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function stevens_mystevens_conditional_items_node_info() {
  $items = array(
    'bookmark' => array(
      'name' => t('Bookmark'),
      'base' => 'node_content',
      'description' => t('A bookmark to be displayed on the MyStevens Portal Page.'),
      'has_title' => '1',
      'title_label' => t('Bookmark Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
