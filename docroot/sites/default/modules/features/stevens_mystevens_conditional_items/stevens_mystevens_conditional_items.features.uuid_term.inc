<?php

/**
 * @file
 * stevens_mystevens_conditional_items.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function stevens_mystevens_conditional_items_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Google Services User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '0347389f-102b-4380-bf40-0d46c2054caa',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Former Student (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '0c344728-042f-414d-a0c0-edfd7c5ce160',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'acctadmin (sitRole)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '189158cf-3e72-4f6d-959a-31b11b46af0a',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Pre-Enroll (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '23eb5faa-70c0-48fb-9e29-f62f801ab572',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'All Users Except GCC User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '254f8187-3083-4685-8496-ffb922985ed0',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Campus Services',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '26197178-d342-465c-afc8-d6cb67847a84',
    'vocabulary_machine_name' => 'mystevens_sections',
  );
  $terms[] = array(
    'name' => 'IT Employee',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '27788c85-74b0-475c-83d7-1201267158dd',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Spaces',
    'description' => '<p>Links to myStevens Spaces.</p>',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '277938fb-67a8-4808-8d32-cc1850e116fc',
    'vocabulary_machine_name' => 'mystevens_sections',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Affiliate (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '2b5f6841-de37-415a-a370-7ccc3157b467',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'admin (sitRole)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '3cd15915-0854-4a3c-a6b5-8373a4bda29c',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Student (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '3f054b56-e390-4b0f-9fbd-f0193859d401',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Password Reset User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '4fff3170-80ef-4430-bb75-ca844fc808a4',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'o365 Mail User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '5e2b7577-49da-4d0e-ae71-e2c254285963',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Google at Stevens',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '66104f6f-b9f6-4282-b250-6892727e2828',
    'vocabulary_machine_name' => 'mystevens_sections',
  );
  $terms[] = array(
    'name' => 'Announcements Admin',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '7de2290f-6ee8-40b0-bb8c-8df9c552d629',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Helpdesk Admin',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '86644f14-926d-4ec7-a4a7-bdfebcac6e0d',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Undergraduate',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '8810d5c3-d84b-498e-ad68-5beb3310fac0',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Google Student Requestable',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '8d70412a-c5f6-41f8-891a-60f7c9ff9a8e',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Admin Tools',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '8f4e565c-2e24-4b10-96bb-a35183a38eec',
    'vocabulary_machine_name' => 'mystevens_sections',
  );
  $terms[] = array(
    'name' => 'Assessment Admin',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '9438e05f-da86-442d-828b-833660ec4a3f',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'GMail User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '9abc100f-e145-456d-af80-0e5249fcd5c5',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Faculty (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => '9e3b67c4-4af6-4316-9e38-84384a6fdc0d',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'GCC Faculty',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'a4e3814e-5335-49dd-bca9-56f3204186ba',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Webassign User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'a5018278-16be-4d08-97cb-ecf1fdfd6732',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Exchange User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'ab753469-c200-4da2-b5c0-6acc2a98f18c',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'GMail Archive User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'b1ca30b4-73b9-4c1a-aff1-4bd4ac5bd087',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Campus Parking Admin',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'bd317acf-349b-489d-8c03-c7b8eaf7605f',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Graduate',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'c119aab8-86ac-4acf-a755-2bee0e3cede1',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'Member (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'cf5bbb79-6567-467c-b7ed-dfcb2993d5d7',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Information Technology Help Desk',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'd47bd9c1-2fdb-4a9f-b51a-cbee38b99027',
    'vocabulary_machine_name' => 'mystevens_sections',
  );
  $terms[] = array(
    'name' => 'Employee (eduPersonAffiliation)',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'e67b8f85-26f4-4599-ba9f-c69668b97db2',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'o365 Portal User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'eab790e5-4d5d-4d95-9b6c-92af0b42dd1d',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  $terms[] = array(
    'name' => 'GCC User',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'f6832377-d5be-4701-be51-41f20900891e',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'NSSE Survey Recipient',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'f9f130b4-b3cf-4f7f-b006-4bdd4b1a28fb',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'All Users',
    'description' => '',
    'format' => 'panopoly_wysiwyg_text',
    'weight' => 0,
    'uuid' => 'ff669b6e-cf18-405d-9d1a-0933bb58131c',
    'vocabulary_machine_name' => 'stevens_mystevens_criteria_sets',
  );
  return $terms;
}
