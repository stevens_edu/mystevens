<?php

/**
 * @file
 * stevens_mystevens_conditional_items.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function stevens_mystevens_conditional_items_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create bookmark content'.
  $permissions['create bookmark content'] = array(
    'name' => 'create bookmark content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bookmark content'.
  $permissions['delete own bookmark content'] = array(
    'name' => 'delete own bookmark content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bookmark content'.
  $permissions['edit own bookmark content'] = array(
    'name' => 'edit own bookmark content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
