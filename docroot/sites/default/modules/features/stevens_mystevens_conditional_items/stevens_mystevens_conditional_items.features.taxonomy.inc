<?php

/**
 * @file
 * stevens_mystevens_conditional_items.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_mystevens_conditional_items_taxonomy_default_vocabularies() {
  return array(
    'mystevens_sections' => array(
      'name' => 'MyStevens Sections',
      'machine_name' => 'mystevens_sections',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'stevens_mystevens_criteria_sets' => array(
      'name' => 'MyStevens Criteria Sets',
      'machine_name' => 'stevens_mystevens_criteria_sets',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
