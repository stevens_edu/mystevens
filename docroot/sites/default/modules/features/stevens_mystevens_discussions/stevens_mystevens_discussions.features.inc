<?php
/**
 * @file
 * stevens_mystevens_discussions.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function stevens_mystevens_discussions_field_default_field_instances_alter(&$data) {
  if (isset($data['comment-comment_node_oa_discussion_post-field_oa_related'])) {
    $data['comment-comment_node_oa_discussion_post-field_oa_related']['settings']['bundle_weights'] = array(); /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-body'])) {
    $data['node-oa_discussion_post-body']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-field_oa_media'])) {
    $data['node-oa_discussion_post-field_oa_media']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
    $data['node-oa_discussion_post-field_oa_media']['settings']['file_extensions'] = 'jpg jpeg gif png txt doc docx xls xlsx pdf ppt pptx pps ppsx odt ods odp mp3 mov mp4 m4a m4v mpeg avi ogg oga ogv weba webp webm ico html htm'; /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-field_oa_related'])) {
    $data['node-oa_discussion_post-field_oa_related']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
    $data['node-oa_discussion_post-field_oa_related']['settings']['bundle_weights'] = array(); /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-oa_section_ref'])) {
    $data['node-oa_discussion_post-oa_section_ref']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-og_group_ref'])) {
    $data['node-oa_discussion_post-og_group_ref']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
  }
  if (isset($data['node-oa_discussion_post-og_vocabulary'])) {
    $data['node-oa_discussion_post-og_vocabulary']['display']['pane'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 0,
    ); /* WAS: '' */
  }
}

/**
 * Implements hook_panelizer_defaults_override_alter().
 */
function stevens_mystevens_discussions_panelizer_defaults_override_alter(&$data) {
  if (isset($data['node:oa_discussion_post:default'])) {
    $data['node:oa_discussion_post:default']->display->content['new-236c7265-d6f0-4868-b0d8-17991e1ec028'] = (object) array(
          'pid' => 'new-236c7265-d6f0-4868-b0d8-17991e1ec028',
          'panel' => 'sidebar',
          'type' => 'og_menu_single_menu',
          'subtype' => 'og_menu_single_menu',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'parent' => 0,
            'return' => 'Finish',
            'cancel' => 'Cancel',
            'override_title' => 0,
            'override_title_text' => '',
            'override_title_heading' => 'h2',
            'og_menu_single_depth' => 0,
            'og_menu_single_parent' => 0,
            'form_build_id' => 'form-yjYeoMv5SUgS4o44K_BCBmu2oidfdfdqcCuCuTMDULQ',
            'form_token' => 'CHmS7iX8YyUEDiLKNcOco0HuB_ir-A2hDxNvUGpUtdc',
            'form_id' => 'og_menu_single_pane_edit_form',
            'op' => 'Finish',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
          'uuid' => '236c7265-d6f0-4868-b0d8-17991e1ec028',
        ); /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->content['new-4753dfae-82f1-435f-83bf-257682e2ab6d'] = (object) array(
          'pid' => 'new-4753dfae-82f1-435f-83bf-257682e2ab6d',
          'panel' => 'contentmain',
          'type' => 'node_comment_wrapper',
          'subtype' => 'node_comment_wrapper',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'mode' => 1,
            'comments_per_page' => 50,
            'context' => 'panelizer',
            'override_title' => 0,
            'override_title_text' => '',
            'override_title_heading' => 'h2',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 2,
          'locks' => array(),
          'uuid' => '4753dfae-82f1-435f-83bf-257682e2ab6d',
        ); /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->content['new-c02b2f30-c874-45f7-8bb2-b81dfb0d6a29'] = (object) array(
          'pid' => 'new-c02b2f30-c874-45f7-8bb2-b81dfb0d6a29',
          'panel' => 'contentmain',
          'type' => 'token',
          'subtype' => 'node:summary',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'sanitize' => 1,
            'context' => 'panelizer',
            'override_title' => 1,
            'override_title_text' => '',
            'override_title_heading' => 'h2',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
          'uuid' => 'c02b2f30-c874-45f7-8bb2-b81dfb0d6a29',
        ); /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->content['new-d7c1a997-b8cd-406c-a940-96d06303f821'] = (object) array(
          'pid' => 'new-d7c1a997-b8cd-406c-a940-96d06303f821',
          'panel' => 'contentmain',
          'type' => 'entity_field',
          'subtype' => 'node:field_oa_media',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'label' => 'above',
            'formatter' => 'file_default',
            'delta_limit' => 0,
            'delta_offset' => 0,
            'delta_reversed' => 0,
            'formatter_settings' => array(),
            'context' => 'panelizer',
            'override_title' => 0,
            'override_title_text' => 'Attachments',
            'override_title_heading' => 'h2',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 1,
          'locks' => array(),
          'uuid' => 'd7c1a997-b8cd-406c-a940-96d06303f821',
        ); /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->layout = 'radix_bryant'; /* WAS: 'radix_burr_flipped' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['content'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['contentfooter'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['contentheader'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['footer'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['header'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['sidebar1'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['sidebar2'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['traybottom'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panel_settings['style_settings']['traytop'] = NULL; /* WAS: '' */
    $data['node:oa_discussion_post:default']->display->panels['contentmain'][0] = 'new-c02b2f30-c874-45f7-8bb2-b81dfb0d6a29'; /* WAS: 'new-a1d93472-6b82-406d-b6b6-22aace42217a' */
    $data['node:oa_discussion_post:default']->display->panels['contentmain'][1] = 'new-d7c1a997-b8cd-406c-a940-96d06303f821'; /* WAS: 'new-bd0d0809-6830-46ab-8f33-272115d12b49' */
    $data['node:oa_discussion_post:default']->display->panels['contentmain'][2] = 'new-4753dfae-82f1-435f-83bf-257682e2ab6d'; /* WAS: 'new-779395e1-2a93-4b60-b511-a461e30ac9df' */
    $data['node:oa_discussion_post:default']->display->panels['sidebar'][0] = 'new-236c7265-d6f0-4868-b0d8-17991e1ec028'; /* WAS: 'new-b9d0d392-f615-4c92-9166-093a30cf05cd' */
    $data['node:oa_discussion_post:default']->display->title = '%node:title'; /* WAS: '' */
    $data['node:oa_discussion_post:default']->pipeline = 'ipe'; /* WAS: 'standard' */
    unset($data['node:oa_discussion_post:default']->display->content['new-08eeb46c-9c1b-4811-9652-2f546cfd67d9']);
    unset($data['node:oa_discussion_post:default']->display->content['new-0af56eac-2101-473d-8c5c-782928eea43b']);
    unset($data['node:oa_discussion_post:default']->display->content['new-5391a87c-2b91-4ee7-a17d-07a99957758b']);
    unset($data['node:oa_discussion_post:default']->display->content['new-779395e1-2a93-4b60-b511-a461e30ac9df']);
    unset($data['node:oa_discussion_post:default']->display->content['new-a1d93472-6b82-406d-b6b6-22aace42217a']);
    unset($data['node:oa_discussion_post:default']->display->content['new-a95c7986-ab4b-4cfc-8bb8-7174a505396a']);
    unset($data['node:oa_discussion_post:default']->display->content['new-b116ce80-5c90-425a-aece-b73ce7992fe3']);
    unset($data['node:oa_discussion_post:default']->display->content['new-b9d0d392-f615-4c92-9166-093a30cf05cd']);
    unset($data['node:oa_discussion_post:default']->display->content['new-bd0d0809-6830-46ab-8f33-272115d12b49']);
    unset($data['node:oa_discussion_post:default']->display->content['new-c0403a30-33da-45c4-b123-96ec401fbd96']);
    unset($data['node:oa_discussion_post:default']->display->content['new-cc5113ed-3c85-45e7-b49c-a5c93ad6e757']);
    unset($data['node:oa_discussion_post:default']->display->panels['contentmain'][3]);
    unset($data['node:oa_discussion_post:default']->display->panels['contentmain'][4]);
    unset($data['node:oa_discussion_post:default']->display->panels['contentmain'][5]);
    unset($data['node:oa_discussion_post:default']->display->panels['contentmain'][6]);
    unset($data['node:oa_discussion_post:default']->display->panels['contentmain'][7]);
    unset($data['node:oa_discussion_post:default']->display->panels['sidebar'][1]);
    unset($data['node:oa_discussion_post:default']->display->panels['sidebar'][2]);
  }
  if (isset($data['node:oa_section:oa_section_discussion'])) {
    $data['node:oa_section:oa_section_discussion']->access['logic'] = 'and'; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->content['new-2959ec85-ade0-4e19-9102-e87704ba0517'] = (object) array(
          'pid' => 'new-2959ec85-ade0-4e19-9102-e87704ba0517',
          'panel' => 'contentmain',
          'type' => 'views_panes',
          'subtype' => 'oa_comment_topics-comment_topics',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'exposed' => array(
              'flagged' => 0,
              'og_group_ref_target_id' => 'CURRENT',
              'og_group_ref_target_id_mine' => 0,
              'og_subspaces_view_all' => 0,
              'og_subspaces_view_parent' => 0,
              'oa_section_ref_target_id' => '',
            ),
            'show_exposed_form' => 0,
            'use_pager' => 1,
            'pager_id' => 1,
            'items_per_page' => 20,
            'override_title' => '',
            'override_title_text' => '',
            'override_title_heading' => 'h2',
            'view_settings' => 'fields',
            'header_type' => 'none',
            'view_mode' => 'teaser',
            'widget_title' => 'Topics',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
          'uuid' => '2959ec85-ade0-4e19-9102-e87704ba0517',
        ); /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->content['new-7cb3f94a-248a-45ef-8318-79ed37da0a54'] = (object) array(
          'pid' => 'new-7cb3f94a-248a-45ef-8318-79ed37da0a54',
          'panel' => 'sidebar',
          'type' => 'og_menu_single_menu',
          'subtype' => 'og_menu_single_menu',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'parent' => 0,
            'return' => 'Finish',
            'cancel' => 'Cancel',
            'override_title' => 0,
            'override_title_text' => '',
            'override_title_heading' => 'h2',
            'og_menu_single_depth' => 0,
            'og_menu_single_parent' => 0,
            'form_build_id' => 'form-FceoQ1XvhuyBJF4NtLay9fONQf12a3m3Uam8_CcFzKg',
            'form_token' => 'CHmS7iX8YyUEDiLKNcOco0HuB_ir-A2hDxNvUGpUtdc',
            'form_id' => 'og_menu_single_pane_edit_form',
            'op' => 'Finish',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
          'uuid' => '7cb3f94a-248a-45ef-8318-79ed37da0a54',
        ); /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->layout = 'radix_bryant'; /* WAS: 'radix_bryant_flipped' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['content'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['contentfooter'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['contentheader'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['footer'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['header'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['sidebar1'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['sidebar2'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['traybottom'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panel_settings['style_settings']['traytop'] = NULL; /* WAS: '' */
    $data['node:oa_section:oa_section_discussion']->display->panels['contentmain'][0] = 'new-2959ec85-ade0-4e19-9102-e87704ba0517'; /* WAS: 'new-b5034a79-ddc6-4fb5-b939-e21cdcfcb6b6' */
    $data['node:oa_section:oa_section_discussion']->display->panels['sidebar'][0] = 'new-7cb3f94a-248a-45ef-8318-79ed37da0a54'; /* WAS: 'new-f40e5cbc-b456-4d18-986e-5d516011a7de' */
    $data['node:oa_section:oa_section_discussion']->display->title = '%node:title'; /* WAS: '' */
    unset($data['node:oa_section:oa_section_discussion']->display->content['new-53a7e967-2673-47b3-86fa-bf985765f6ca']);
    unset($data['node:oa_section:oa_section_discussion']->display->content['new-6e7b549b-c4e3-4462-b4d2-a02b9db9eff4']);
    unset($data['node:oa_section:oa_section_discussion']->display->content['new-a3f6416e-1faf-4899-ae8d-c398767509dd']);
    unset($data['node:oa_section:oa_section_discussion']->display->content['new-b5034a79-ddc6-4fb5-b939-e21cdcfcb6b6']);
    unset($data['node:oa_section:oa_section_discussion']->display->content['new-f40e5cbc-b456-4d18-986e-5d516011a7de']);
    unset($data['node:oa_section:oa_section_discussion']->display->panels['contentmain'][1]);
    unset($data['node:oa_section:oa_section_discussion']->display->panels['contentmain'][2]);
    unset($data['node:oa_section:oa_section_discussion']->display->panels['contentmain'][3]);
  }
}
