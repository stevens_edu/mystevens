jQuery( document ).ready(function() {   
  // Get all slides on the page
  var all_slides = jQuery(".flex-viewport .slides li");

  // For each slide,
  all_slides.each(function( index ) {

    // Get the target URL of this slide's caption (if one exists).
    caption_target_link = jQuery(this).find(".flex-caption a");

    // If the target_url exists
    if (caption_target_link != undefined) {
    
      // Get the image element of this slide
      slide_image = jQuery(this).find("img");
      caption_div = jQuery(this).find(".flex-caption");

      // Remove the inner HTML of the slide_target_link,
      // wrap the image and the caption div in an a tag
      // with an href of the target_url 
      image_target_link = caption_target_link.clone();
      image_target_link.html("");
      slide_image.wrap(image_target_link);
      caption_div.wrap(image_target_link);
    }
  });
});
