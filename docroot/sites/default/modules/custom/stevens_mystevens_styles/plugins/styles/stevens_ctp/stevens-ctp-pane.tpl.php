<section class="mystevens-pane <?php print ($settings['is_menu']) ? 'stevens-ctp-menu' : ''; ?> <?php print ($settings['is_spotlight']) ? 'pane-bundle-spotlight' : ''; ?>">
  <?php if (isset($content->title) && trim($content->title) != ""): ?>
    <div class="mystevens-pane-title <?php print (isset($settings['header_size'])) ? $settings['header_size'] : 'large-header'; ?>">
      <h2>
        <?php print $content->title; ?>
      </h2>
    </div>
    <?php if ($settings['is_spotlight'] == FALSE): ?>
      <div class="arrow-down <?php print ($settings['header_size'] == 'large-header') ? 'large-arrow' : 'small-arrow'; ?>"></div>
    <?php endif; ?>
  <?php endif; ?>
  <div class="mystevens-pane-content <?php print $settings['pane_content_classes']; ?>">
    <?php print render($content->content); ?>
  </div>
</section>
