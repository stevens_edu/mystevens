<?php

/**
* Implementation of hook_panels_styles().
*/
$plugin =  array(
  'stevens_ctp' => array(
    'title' => t('Default Pane Style'),
    'description'   => t('This is the default pane style.  It employs a "Stevens Red" header of custom size and a custom background color.'),
    'render pane' => 'stevens_ctp_render_pane',
    'pane settings form' => 'stevens_ctp_settings_form',
    'hook theme'    => array(
      'mystevens_theme_pane' => array(
        'template' => 'stevens-ctp-pane',
         'path' => drupal_get_path('module', 'stevens_mystevens_styles') .'/plugins/styles/stevens_ctp',
         'variables' => array(
           'content' => NULL,
           'settings' => NULL,
         ),
       ),
    ),
  ),
);

/**
 * Callback render function for mystevens pane.
 */
function theme_stevens_ctp_render_pane($vars) {
  $settings = $vars['settings'];
  $content = $vars['content'];

  $pane_content_classes = array(); 

  // Always add a clearfix class for, e.g., wysiwyg floats.
  $pane_content_classes[] = 'clearfix';

  //Is this for a menu pane?
  //TODO: Check for menu programatically
  if ($settings['is_menu'] == 1) {
    $pane_content_classes[] = "mystevens-menu-pane";
  }

  //If this is a spotlight pane, apply only the spotlight related class
  //and not large- or small- pane
  //@TODO: Check for spotlight programatically
  if ($settings['is_spotlight'] == 1) {
    $pane_content_classes[] = "mystevens-spotlight-pane";
  }
  else if  (isset($settings['header_size']) && $settings['header_size'] == 'small-header') {
    $pane_content_classes[] = "small-pane";  
  }
  else {
    //Default to large pane styles
    $pane_content_classes[] = "large-pane";  
  }

  if(isset($settings['bg_color'])){
    $pane_content_classes[] = $settings['bg_color'];
  }
  else {
    $pane_content_classes[] = 'gray-bg';
  }

  $settings['pane_content_classes'] = implode(' ', $pane_content_classes);

  return theme('mystevens_theme_pane', array('content' => $content, 'settings' => $settings));
}

/**
 * Callback settings function for mystevens pane.
 */
function stevens_ctp_settings_form($style_settings) {
  $form['bg_color'] = array(
    '#type' => 'select',
    '#title' => t('Background color'),
    '#options' => array(
      'white-bg' => t('White'),
      'gray-bg' => t('Light Gray (Default)'),
      'black-bg' => t('Black'),
    ),
    '#default_value' => (isset($style_settings['bg_color'])) ? $style_settings['bg_color'] : 'gray-bg',
  );

  $form['header_size'] = array(
    '#type' => 'select',
    '#title' => t('Header Size'),
    '#options' => array(
      'large-header' => t('Large (Default)'),
      'small-header' => t('Small'),
    ),
    '#default_value' => (isset($style_settings['header_size'])) ? $style_settings['header_size'] : 'large-header',
  );

  $form['is_menu'] = array(
    '#type' => 'checkbox',
    '#title' => t('Menu Pane?'),
    '#return_value' => 1,
    '#default_value' => (isset($style_settings['is_menu'])) ? $style_settings['is_menu'] : 0,
    '#description' => t("Does or will this pane contain a Menu?"),
  );

  $form['is_spotlight'] = array(
    '#type' => 'checkbox',
    '#title' => t('Spotlight Pane?'),
    '#return_value' => 1,
    '#default_value' => (isset($style_settings['is_spotlight'])) ? $style_settings['is_spotlight'] : 0,
    '#description' => t("Does or will this pane contain a Spotlight carousel?"),
  );
 
  return $form;
}
