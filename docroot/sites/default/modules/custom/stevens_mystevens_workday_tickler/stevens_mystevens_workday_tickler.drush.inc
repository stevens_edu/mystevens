<?php
/**
 * @file
 * Drush integration for Workday Tickler module.
 */

/**
 * Implements hook_drush_command().
 */
function stevens_mystevens_workday_tickler_drush_command() {
  $commands['wt-resetdeferrals'] = array(
    'description' => 'Resets tickle_status to NULL if it was Deferred.',
    'aliases' => array('wt-rd'),
    'examples' => array(
      'drush @my.[env] wt-rd' => 'Resets tickle_status to NULL if it was Deferred.',
    ),
  );

  $commands['wt-setstatus'] = array(
    'description' => 'Sets tickle_status to the provided value (NULL is actual NULL).',
    'aliases' => array('wt-sts'),
    'examples' => array(
      'drush @my.[env] wt-sts username [Completed|Deferred|NULL]' => 'Sets tickle_status to the provided value (NULL is actual NULL).',
    ),
    'arguments' => array(
      'username' => 'The username of the user.',
      'tickle_status' => 'The desired tickle_status value.',
    ),
  );

  $commands['wt-setbucket'] = array(
    'description' => 'Sets tickle_bucket to the provided value (NULL is actual NULL).',
    'aliases' => array('wt-stb'),
    'examples' => array(
      'drush @my.[env] wt-stb username [AllContactLoaded|ContactError|StudentWorker|NULL]' => 'Sets tickle_bucket to the provided value (NULL is actual NULL).',
    ),
    'arguments' => array(
      'username' => 'The username of the user.',
      'tickle_bucket' => 'The desired tickle_bucket value.',
    ),
  );

  $commands['wt-getstatus'] = array(
    'description' => 'Gets tickle_status of a user.',
    'aliases' => array('wt-gts'),
    'examples' => array(
      'drush @my.[env] wt-gts username' => 'Gets tickle_status of a user.',
    ),
    'arguments' => array(
      'username' => 'The username of the user.',
    ),
  );

  $commands['wt-getbucket'] = array(
    'description' => 'Gets tickle_bucket of a user.',
    'aliases' => array('wt-gtb'),
    'examples' => array(
      'drush @my.[env] wt-gtb username' => 'Gets tickle_bucket of a user.',
    ),
    'arguments' => array(
      'username' => 'The username of the user.',
    ),
  );

  return $commands;
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_stevens_mystevens_workday_tickler_wt_resetdeferrals() {
  $result = _stevens_mystevens_workday_tickler_reset_deferrals();
  drush_log(dt('Reset ' . $result . ' deferrals.'), 'ok');
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_stevens_mystevens_workday_tickler_wt_setstatus($username, $tickle_status) {
  $result = _stevens_mystevens_workday_tickler_setstatus($username, $tickle_status);
  drush_log(dt('Tickle Status set.'), 'ok');
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_stevens_mystevens_workday_tickler_wt_setbucket($username, $tickle_bucket) {
  $result = _stevens_mystevens_workday_tickler_setbucket($username, $tickle_bucket);
  drush_log(dt('Tickle Bucket set.'), 'ok');
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_stevens_mystevens_workday_tickler_wt_getstatus($username) {
  $result = _stevens_mystevens_workday_tickler_getstatus($username);
  drush_log(dt('Tickle Status: ' . $result), 'ok');
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_stevens_mystevens_workday_tickler_wt_getbucket($username) {
  $result = _stevens_mystevens_workday_tickler_getbucket($username);
  drush_log(dt('Tickle Bucket: ' . $result), 'ok');
}
