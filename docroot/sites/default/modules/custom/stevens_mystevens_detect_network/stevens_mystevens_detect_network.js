(function ($) {
  var timer;
  var keepPolling = true;
  var onStevens = true;

  function offToOnStevens() {
    // Everything that should occur when transitioning onto the Stevens network.

    toggleBookmark("799b7ac4-28e7-49b2-83ec-44d849c8a6f1", true); // enable Facilities Workorder
    toggleBookmark("de97c3f3-c95e-4edd-a90f-d67582dacbf8", true); // enable MAC Address Registration
  }

  function onToOffStevens() {
    // Everything that should occur when transitioning off of the Stevens network.

    toggleBookmark("799b7ac4-28e7-49b2-83ec-44d849c8a6f1", false); // disable Facilities Workorder
    toggleBookmark("de97c3f3-c95e-4edd-a90f-d67582dacbf8", false); // disable MAC Address Registration
  }

  function toggleBookmark(uuid, state) {
    if (state) {
      jQuery("#badge-" + uuid).parent().parent().parent()
        .fadeTo("slow", 1.0)
        .find('a').removeAttr('onClick');
    }
    else {
      jQuery("#badge-" + uuid).parent().parent().parent()
        .fadeTo("slow", 0.4)
        .find('a').attr('onClick', 'alert("This service requires a Stevens network connection."); return false;');
    }
  }

  jQuery(document).ready(function () {
    getIP();

    function getIP() {
      jQuery.ajax({
        type: 'POST',
        url: "https://api.stevens.edu/esp/ip",
        data: JSON.stringify(Drupal.settings.stevens_esp.creds),
        success: function (data) {
                   if (data.ip.match("^(155\\.246\\.|192\\.168\\.|10\\.)")) {
                     if (!onStevens) offToOnStevens();
                     onStevens = true;
                   }
                   else {
                     if (onStevens) onToOffStevens();
                     onStevens = false;
                   }

                 },
         contentType: "application/json",
         dataType: 'json'
       }).always(function() {
                   if (keepPolling) {
                     timer = setTimeout(function() {
                       getIP();
                     }, Drupal.settings.stevens_esp.pollingInterval);
                   }
                 });
    }

    jQuery(window).focus(function () {
      keepPolling = true;
      clearTimeout(timer); // There's usually a blur() before a
      // focus() so this is already cleared, but it's possible to have a
      // focus() first (which would cause double polling without this clear)
      // if the window loses focus before this script's document.ready().
      getIP();

    }).blur(function () {
      keepPolling = false;
      clearTimeout(timer);
    });
  });
})(jQuery);
