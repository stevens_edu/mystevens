jQuery(document).ready(function () {
  function resetAll() {
    jQuery('#form')[0].reset();
    jQuery('.error').css('display', 'none');
    jQuery('#inputName').css('border-color', '#ccc');
    jQuery('#email').css('border-color', '#ccc');
    jQuery('#comment').css('border-color', '#ccc');
  }
  jQuery('.reset').click(function() {
    resetAll();
  });

  function validateForm() {
    var fen = 0;
    var fee = 0;
    var fec = 0;
    var username = jQuery('#inputName').val();
    var feedback = jQuery('#comment').val();
    if (feedback == "") {
      jQuery('#textbox-error').css('display', 'block');
      jQuery('#textbox-error').css('color', 'red');
      jQuery('#comment').css('border-color', '#f05544');
      fec = 1;
    }
    if (feedback != "") {
      jQuery('#textbox-error').css('display', 'none');
      jQuery('#comment').css('border-color', '#ccc');
      fec = 0;
    }
    if (fen == 1 || fee == 1 || fec == 1) {
      return false;
    }
    return true;
  }
  jQuery('#submit-button').click(function(e) {
    e.preventDefault();
    if (validateForm() === false) {
      return false;
    }
    var message = jQuery("#comment").val();
    jQuery.ajax({
      type: "POST",
      url: "feedback-handler",
      data: "message=" + encodeURIComponent(message),
      success:function(result)
      {
        if (result == "true") {
          jQuery("#myModal").modal('hide');
          jQuery('#thank-you').slideDown(1000, function() {
            setTimeout(function(){ 
              jQuery("#thank-you").slideUp(3000);
            },3000);
          });
          resetAll();
        }
        else{
          jQuery("#myModal").modal('hide');
          jQuery('#db-error').slideDown(1000, function() {
          setTimeout(function(){  jQuery("#db-error").slideUp(3000); }, 3000);
          });
        } 
      }
    });
    return false;
  });
}); 
