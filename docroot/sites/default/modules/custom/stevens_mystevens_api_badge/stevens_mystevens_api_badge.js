(function ($) {
  var timers = {};
  var keepPolling = true;

  jQuery(document).ready(function () {
    jQuery.each(Drupal.settings.stevens_esp.badgeEndpoints, function (uuid, endpoint) {
      getBadge(uuid, endpoint);
    });

    function getBadge(uuid, endpoint) {
      jQuery.ajax({
        type: 'POST',
        url: "https://api.stevens.edu/esp/badge/" + endpoint,
        data: JSON.stringify(Drupal.settings.stevens_esp.creds),
        success: function (data) {
                   jQuery.each(data, function(key, content){
                     if (content == 0) {
                       jQuery("#badge-" + uuid).fadeOut(1500);
                     }
                     else {
                       jQuery("#badge-" + uuid).fadeIn(1500).html(content);
                     }
                   });
                 },
         contentType: "application/json",
         dataType: 'json'
       }).always(function() {
                   if (keepPolling) {
                     timers[endpoint] = setTimeout(function() {
                       getBadge(uuid, endpoint);
                     }, Drupal.settings.stevens_esp.pollingInterval);
                   }
                 });
    }

    jQuery(window).focus(function () {
      keepPolling = true;
      jQuery.each(Drupal.settings.stevens_esp.badgeEndpoints, function (uuid, endpoint) {
        clearTimeout(timers[endpoint]); // There's usually a blur() before a
        // focus() so this is already cleared, but it's possible to have a
        // focus() first (which would cause double polling without this clear)
        // if the window loses focus before this script's document.ready().
        getBadge(uuid, endpoint);
      });

    }).blur(function () {
      keepPolling = false;
      jQuery.each(timers, function (endpoint, timerID) {
        clearTimeout(timerID);
      });
    });
  });
})(jQuery);
