No Sender
---------

No Sender suppresses the inclusion of a Sender header in email generated from
the site. The Mime Mail module also happens to do what this module does, amidst
plenty of other features. Sites without one of these modules (or custom code to
alter the behavior) will send email with the Sender header set to the site admin
email address (the "Site Details" > "E-mail address" field at
admin/config/system/site-information) even for messages where the From header is
set otherwise, for example via Webform configuration.

Mail clients render the Sender header on received mail in various ways,
generally very near to the From header. Some will actually label them discretely
("From: [from address]" / "Sender: [sender address]") while others will combine
the two addresses into a single phrase ("[from address] sent by [sender
address]" or "[sender address] on behalf of [from address]" or "[from address]
via [sender address]"). Furthermore, the "Block Sender" feature in some mail
clients will actually add the sender address to the user's blacklist, which can
mean that all mail from your site gets blacklisted when perhaps the user only
intended to block the from address (a subset of mail from your site).

When to use No Sender
---------------------

Use this module if:

  * You want mail from your site to not reveal the site admin email address.
  * You want recipients to see one address ("From") instead of two ("From" and
    "Sender").
  * You want to strictly follow the email RFC which says that if the From and
    Sender headers would be the same, the latter should be omitted.
  * You want to strictly follow the email RFC which says that the Sender header
    should be set to a value representing the agent actually transmitting the
    message, but you feel as though the value of your From address best
    represents that agent.

Do not use this module if:

  * You want to strictly follow the email RFC which says that the Sender header
    should be set to a value representing the agent actually transmitting the
    message, and you feel as though your site admin email address best
    represents that agent.
