<?php
/**
 * @file
 * Implementation of the abstract sanitization procedure.
 *
 * Note that all sanitization procedures are managed as singletons, and
 * instantiated via sanitize_procedure().
 *
 * @see sanitize_procedure().
 */

/**
 * Exception thrown if the sanitization process fails.
 */
class SanitizeProcedureException extends Exception {}

/**
 * Abstract class defining a sanitization procedure.
 */
abstract class SanitizeProcedure {
  /**
   * Instantiates a sanitization procedure, based on a procedure name.
   *
   * Note that all sanitization procedures are managed as singletons, and
   * instantiated via sanitize_procedure().
   *
   * @see sanitize_procedure().
   */
  public static function instance($procedure) {
    if (!strlen($procedure)) {
      throw new InvalidArgumentException(t('Unable to get SanitizeProcedure instance: Procedure class name is required.'));
    }

    $instances = &drupal_static(__METHOD__, array());

    if (!isset($instances[$procedure])) {
      $instances[$procedure] = new $procedure();
    }
    return $instances[$procedure];
  }

  /**
   * Conduct sanitization of the current site.
   */
  public abstract function sanitize();

  /**
   * Prepare the export SQL before being written to a file.
   *
   * @param resource $handle
   *   A file handle for the temporary SQL file to be written.
   */
  public abstract function prepareExport($handle);

  /**
   * Perform additional cleanup following sanitization.
   */
  public abstract function postSanitize();

  /**
   * Helper function to truncate a table if it exists.
   *
   * @param string $table
   *   The name of the table.
   */
  protected function trunc($table) {
    if (db_table_exists($table)) {
      $result = db_truncate($table)->execute();
    }
  }
}
