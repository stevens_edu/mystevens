<?php
/**
 * @file
 * Implementation of the MyStevens sanitization procedure.
 */

/**
 * Class defining a sanitization procedure for MyStevens.
 */
class SanitizeMyStevensProcedure extends SanitizeDefaultProcedure {
  /**
   * Conduct sanitization of the current site.
   */
  public function sanitize() {
    drush_print(dt('Removing private and unpublished spaces and sections.'));
    $private_nodes = array();
    $private_q = db_query('SELECT DISTINCT n.nid 
      FROM node_access na, node n 
      WHERE n.nid=na.nid 
      AND (n.type = :oa_section OR n.type = :oa_space) 
      AND (na.realm = :og_access OR n.status = 0)', 
      array(':oa_section' => 'oa_section', ':oa_space' => 'oa_space', ':og_access' => 'og_access:node'));
    $private_res = $private_q->fetchAll();
    foreach ($private_res as $row) {
      drush_print(dt('  Deleting @nid.', array('@nid' => $row->nid)));
      $private_nodes[] = $row->nid;
      $node = node_load($row->nid);
      // Flag orphans for deletion.
      oa_core_store_orphan_settings($node, array('og_orphans' => array('delete' => 'delete')));    
    }
    node_delete_multiple($private_nodes);

    drush_print(dt('Performing cleanup of orphaned content...'));
    drush_invoke_process('@self', 'queue-run', array('og_membership_orphans'), array(), TRUE);

    drush_print(dt('Removing unpublished nodes.'));
    $unpub_nodes = array();
    $unpub_q = db_query('SELECT n.nid 
      FROM node n 
      WHERE status=0;'); 
    $unpub_res = $unpub_q->fetchAll();
    foreach ($unpub_res as $row) {
      drush_print(dt('  Deleting @nid.', array('@nid' => $row->nid)));
      $unpub_nodes[] = $row->nid;
    }
    node_delete_multiple($unpub_nodes);

    drush_print(dt('Scrubbing ESP salt.'));
    variable_del('esp_salt');

    drush_print(dt('Scrubbing addweb secret.'));
    variable_del('addweb_secret');

    drush_print(dt('Scrubbing Acquia secrets.'));
    variable_del('ah_network_key');
    variable_del('acquia_key');
    variable_del('acquia_identifier');
    variable_del('acquia_subscription_data');
    variable_del('acquia_search_key');
    variable_del('acquia_search_host');
    variable_del('acquia_search_derived_key_salt');

    drush_print(dt('Truncating search indices and configuration.'));
    variable_del('apachesolr_default_enviornments');
    variable_del('apachesolr_enviornments');
    variable_del('apachesolr_default_search_page');
    variable_del('apachesolr_search_mlt_blocks');

    $this->trunc('search_dataset');
    $this->trunc('search_index');
    $this->trunc('search_node_links');
    $this->trunc('search_total');
    $this->trunc('search_api_db_database_node_index');
    $this->trunc('search_api_db_database_node_index_field_oa_related');
    $this->trunc('search_api_db_database_node_index_oa_section_ref_title');
    $this->trunc('search_api_db_database_node_index_og_group_ref_title');
    $this->trunc('search_api_db_database_node_index_search_api_access_node');
    $this->trunc('search_api_db_database_node_index_text');
    $this->trunc('search_api_db_user_index');
    $this->trunc('search_api_db_user_index_name');
    $this->trunc('search_api_db_user_index_og_user_node');
    $this->trunc('search_api_db_user_index_search_api_language');
    $this->trunc('search_api_db_user_index_search_api_url');
    $this->trunc('search_api_db_user_index_text');
    $this->trunc('search_api_index');
    $this->trunc('search_api_item');
    $this->trunc('search_api_item_string_id');
    $this->trunc('search_api_server');
    $this->trunc('search_api_task');
    $this->trunc('apachesolr_environment');
    $this->trunc('apachesolr_environment_variable');
    $this->trunc('apachesolr_index_bundles');
    $this->trunc('apachesolr_index_entities');
    $this->trunc('apachesolr_index_entities_node');
    $this->trunc('apachesolr_search_page');

    drush_print(dt('Deleting private file metadata (excepting that managed by Sanitize).'));
    $result = db_delete('file_managed')
      ->condition('uri', 'private://%', 'LIKE')
      ->condition('uri', 'private://sanitize/%', 'NOT LIKE')
      ->execute();

    parent::sanitize();
  }

  /**
   * Prepare the export SQL before being written to a file.
   *
   * @param resource $handle
   *   A file handle for the temporary SQL file to be written.
   */
  public function prepareExport($handle) {
    parent::prepareExport($handle);
  }

  /**
   * Perform additional cleanup following sanitization.
   */
  public function postSanitize() { 
    parent::postSanitize();
  }
}
