; Open Atrium Related Content Makefile

api = 2
core = 7.x

; References Dialog
projects[references_dialog][version] = 1.0-beta1
projects[references_dialog][subdir] = contrib
projects[references_dialog][patch][2315905] = http://drupal.org/files/issues/references_dialog_fix_theme_links-2315905-5.patch.patch
projects[references_dialog][patch][2375741] = http://drupal.org/files/issues/2375741-fixing_typo-1.patch
; References Related Content App
projects[oa_related][version] = 2.10
projects[oa_related][patch] = https://www.drupal.org/files/issues/2019-09-27/oa_related-php72-3075225-4.patch
