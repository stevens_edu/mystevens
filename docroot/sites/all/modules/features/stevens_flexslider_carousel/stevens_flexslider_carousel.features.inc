<?php
/**
 * @file
 * stevens_flexslider_carousel.features.inc
 */

/**
 * Implements hook_default_command_button().
 */
function stevens_flexslider_carousel_default_command_button() {
  $items = array();
  $items['carousel_banner_image'] = entity_import('command_button', '{
    "bundle" : "node_add",
    "name" : "carousel_banner_image",
    "title" : "Create Carousel Banner Image",
    "language" : "und",
    "field_command_link" : { "und" : [
        {
          "url" : "node\\/add\\/carousel-banner-image",
          "title" : "Create Carousel Banner Image",
          "attributes" : []
        }
      ]
    }
  }');
  $items['carousel_container'] = entity_import('command_button', '{
    "bundle" : "node_add",
    "name" : "carousel_container",
    "title" : "Create Carousel Container",
    "language" : "und",
    "field_command_link" : { "und" : [
        {
          "url" : "node\\/add\\/carousel-container",
          "title" : "Create Carousel Container",
          "attributes" : []
        }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_flexslider_carousel_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stevens_flexslider_carousel_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flexslider_default_presets_alter().
 */
function stevens_flexslider_carousel_flexslider_default_presets_alter(&$data) {
  if (isset($data['default'])) {
    $data['default']->options['animation'] = 'slide'; /* WAS: 'fade' */
    $data['default']->options['animationLoop'] = 1; /* WAS: TRUE */
    $data['default']->options['directionNav'] = 1; /* WAS: TRUE */
    $data['default']->options['easing'] = 'linear'; /* WAS: 'swing' */
    $data['default']->options['keyboard'] = 1; /* WAS: TRUE */
    $data['default']->options['mousewheel'] = 0; /* WAS: FALSE */
    $data['default']->options['multipleKeyboard'] = 0; /* WAS: FALSE */
    $data['default']->options['pauseOnAction'] = 1; /* WAS: TRUE */
    $data['default']->options['pauseOnHover'] = 1; /* WAS: FALSE */
    $data['default']->options['pausePlay'] = 0; /* WAS: FALSE */
    $data['default']->options['randomize'] = 0; /* WAS: FALSE */
    $data['default']->options['reverse'] = 0; /* WAS: FALSE */
    $data['default']->options['slideshow'] = 1; /* WAS: TRUE */
    $data['default']->options['smoothHeight'] = 0; /* WAS: FALSE */
    $data['default']->options['thumbCaptions'] = 0; /* WAS: FALSE */
    $data['default']->options['thumbCaptionsBoth'] = 0; /* WAS: FALSE */
    $data['default']->options['touch'] = 1; /* WAS: TRUE */
    $data['default']->options['useCSS'] = 1; /* WAS: TRUE */
    $data['default']->options['video'] = 0; /* WAS: FALSE */
    unset($data['default']->export_type);
    unset($data['default']->type);
  }
}

/**
 * Implements hook_image_styles_alter().
 */
function stevens_flexslider_carousel_image_styles_alter(&$data) {
  if (isset($data['flexslider_full'])) {

  if (!isset($data['flexslider_full']['storage']) || $data['flexslider_full']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['flexslider_full']['effects'] = array_values($data['flexslider_full']['effects']);
    $data['flexslider_full']['label'] = 'flexslider_full';
  }
  }
  if (isset($data['flexslider_thumbnail'])) {

  if (!isset($data['flexslider_thumbnail']['storage']) || $data['flexslider_thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['flexslider_thumbnail']['effects'] = array_values($data['flexslider_thumbnail']['effects']);
    $data['flexslider_thumbnail']['label'] = 'flexslider_thumbnail';
  }
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function stevens_flexslider_carousel_views_default_views_alter(&$data) {
  if (isset($data['flexslider_carousel'])) {
    $data['flexslider_carousel']->display['panel_pane_1']->display_options['arguments'] = array(
      'field_oa_group_ref_target_id' => array(
        'id' => 'field_oa_group_ref_target_id',
        'table' => 'field_data_field_oa_group_ref',
        'field' => 'field_oa_group_ref_target_id',
        'default_action' => 'default',
        'default_argument_type' => 'og_user_groups',
        'summary' => array(
          'number_of_records' => 0,
          'format' => 'default_summary',
        ),
        'summary_options' => array(
          'items_per_page' => 25,
        ),
        'break_phrase' => TRUE,
      ),
    ); /* WAS: '' */
    $data['flexslider_carousel']->display['panel_pane_1']->display_options['defaults']['arguments'] = FALSE; /* WAS: '' */
    $data['flexslider_carousel']->display['panel_pane_1']->display_options['defaults']['query'] = FALSE; /* WAS: '' */
    $data['flexslider_carousel']->display['panel_pane_1']->display_options['query']['options']['distinct'] = TRUE; /* WAS: '' */
    unset($data['flexslider_carousel']->display['default']->display_options['pager']['options']['offset']);
    unset($data['flexslider_carousel']->display['panel_pane_1']->display_options['sorts']['weight']);
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_flexslider_carousel_node_info() {
  $items = array(
    'carousel_banner_image' => array(
      'name' => t('Carousel Banner Image'),
      'base' => 'node_content',
      'description' => t('Used to create banner images to be included in Carousel Containers.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'carousel_container' => array(
      'name' => t('Carousel Container'),
      'base' => 'node_content',
      'description' => t('Used to create carousel containers which contain Carousel Banner Images.  Uses FlexSlider to render.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
