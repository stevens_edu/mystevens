<?php
/**
 * @file
 * stevens_flexslider_carousel.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_flexslider_carousel_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'carousel_containers_by_space';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Carousel Containers by Space';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: OG membership: OG membership from Content */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['gid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['gid']['validate']['type'] = 'og';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'carousel_container' => 'carousel_container',
  );

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['carousel_containers_by_space'] = $view;

  $view = new view();
  $view->name = 'flexslider_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Flexslider Carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['captionfield'] = 'field_carousel_banner_link';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'pane';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Banner Image */
  $handler->display->display_options['fields']['field_carousel_banner_image']['id'] = 'field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['table'] = 'field_data_field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['field'] = 'field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_banner_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_banner_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_banner_image']['type'] = 'flexslider';
  $handler->display->display_options['fields']['field_carousel_banner_image']['settings'] = array(
    'optionset' => 'default',
    'image_style' => 'panopoly_image_spotlight',
    'caption' => 0,
  );
  $handler->display->display_options['fields']['field_carousel_banner_image']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_banner_image']['delta_limit'] = '10';
  $handler->display->display_options['fields']['field_carousel_banner_image']['delta_offset'] = '0';
  /* Field: Content: Banner Text & Link */
  $handler->display->display_options['fields']['field_carousel_banner_link']['id'] = 'field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['table'] = 'field_data_field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['field'] = 'field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_banner_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_banner_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_banner_link']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Weight (field_carousel_banner_weight) */
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['id'] = 'field_carousel_banner_weight_value';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['table'] = 'field_data_field_carousel_banner_weight';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['field'] = 'field_carousel_banner_weight_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'carousel_banner_image' => 'carousel_banner_image',
  );
  /* Filter criterion: Field: Groups audience (og_group_ref:target_id) */
  $handler->display->display_options['filters']['og_group_ref_target_id']['id'] = 'og_group_ref_target_id';
  $handler->display->display_options['filters']['og_group_ref_target_id']['table'] = 'og_membership';
  $handler->display->display_options['filters']['og_group_ref_target_id']['field'] = 'og_group_ref_target_id';
  $handler->display->display_options['filters']['og_group_ref_target_id']['value'] = 'myStevens Portal - 12446';
  $handler->display->display_options['filters']['og_group_ref_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['og_group_ref_target_id']['expose']['operator_id'] = 'og_group_ref_target_id_op';
  $handler->display->display_options['filters']['og_group_ref_target_id']['expose']['label'] = 'Space';
  $handler->display->display_options['filters']['og_group_ref_target_id']['expose']['operator'] = 'og_group_ref_target_id_op';
  $handler->display->display_options['filters']['og_group_ref_target_id']['expose']['identifier'] = 'og_group_ref_target_id';
  $handler->display->display_options['filters']['og_group_ref_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    31 => 0,
  );
  /* Filter criterion: Content: Carousel (field_carousel:target_id) */
  $handler->display->display_options['filters']['field_carousel_target_id']['id'] = 'field_carousel_target_id';
  $handler->display->display_options['filters']['field_carousel_target_id']['table'] = 'field_data_field_carousel';
  $handler->display->display_options['filters']['field_carousel_target_id']['field'] = 'field_carousel_target_id';
  $handler->display->display_options['filters']['field_carousel_target_id']['value'] = array(
    24076 => '24076',
    24071 => '24071',
  );
  $handler->display->display_options['filters']['field_carousel_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['operator_id'] = 'field_carousel_target_id_op';
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['label'] = 'Carousel';
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['operator'] = 'field_carousel_target_id_op';
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['identifier'] = 'field_carousel_target_id';
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_carousel_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
    16 => 0,
    21 => 0,
    31 => 0,
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'flexslider_carousel:panel_pane_2';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Sort criterion: Content: Weight (field_carousel_banner_weight) */
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['id'] = 'field_carousel_banner_weight_value';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['table'] = 'field_data_field_carousel_banner_weight';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['field'] = 'field_carousel_banner_weight_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['pane_category']['name'] = 'Carousels';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['exposed_form_configure'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['allow']['panopoly_magic_display_type'] = 0;
  $handler->display->display_options['exposed_form_overrides'] = array(
    'filters' => array(
      'og_group_ref_target_id' => '',
      'field_carousel_target_id' => '',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Banner Sorting Content Pane */
  $handler = $view->new_display('panel_pane', 'Banner Sorting Content Pane', 'panel_pane_2');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'og_context';
  $handler->display->display_options['access']['perm'] = 'administer panelizer og_group content';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'field_carousel_banner_image' => 'field_carousel_banner_image',
    'field_carousel_banner_link' => 'field_carousel_banner_link',
    'draggableviews' => 'draggableviews',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_carousel_banner_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_carousel_banner_link' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'draggableviews' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Banner Text & Link */
  $handler->display->display_options['fields']['field_carousel_banner_link']['id'] = 'field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['table'] = 'field_data_field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['field'] = 'field_carousel_banner_link';
  $handler->display->display_options['fields']['field_carousel_banner_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_banner_link']['click_sort_column'] = 'url';
  /* Field: Content: Banner Image */
  $handler->display->display_options['fields']['field_carousel_banner_image']['id'] = 'field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['table'] = 'field_data_field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['field'] = 'field_carousel_banner_image';
  $handler->display->display_options['fields']['field_carousel_banner_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_banner_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_banner_image']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => 'content',
  );
  /* Field: Draggableviews: Content */
  $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
  $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'flexslider_carousel:panel_pane_2';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Sort criterion: Content: Weight (field_carousel_banner_weight) */
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['id'] = 'field_carousel_banner_weight_value';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['table'] = 'field_data_field_carousel_banner_weight';
  $handler->display->display_options['sorts']['field_carousel_banner_weight_value']['field'] = 'field_carousel_banner_weight_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['pane_category']['name'] = 'Carousels';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['exposed_form_configure'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['allow']['panopoly_magic_display_type'] = 0;
  $handler->display->display_options['exposed_form_overrides'] = array(
    'filters' => array(
      'og_group_ref_target_id' => '',
      'field_carousel_target_id' => '',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['flexslider_carousel'] = $view;

  return $export;
}
