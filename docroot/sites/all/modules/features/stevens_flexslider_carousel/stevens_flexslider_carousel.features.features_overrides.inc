<?php
/**
 * @file
 * stevens_flexslider_carousel.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function stevens_flexslider_carousel_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: flexslider_optionset
  $overrides["flexslider_optionset.default.export_type"]["DELETED"] = TRUE;
  $overrides["flexslider_optionset.default.options|animation"] = 'slide';
  $overrides["flexslider_optionset.default.options|animationLoop"] = 1;
  $overrides["flexslider_optionset.default.options|directionNav"] = 1;
  $overrides["flexslider_optionset.default.options|easing"] = 'linear';
  $overrides["flexslider_optionset.default.options|keyboard"] = 1;
  $overrides["flexslider_optionset.default.options|mousewheel"] = 0;
  $overrides["flexslider_optionset.default.options|multipleKeyboard"] = 0;
  $overrides["flexslider_optionset.default.options|pauseOnAction"] = 1;
  $overrides["flexslider_optionset.default.options|pauseOnHover"] = 1;
  $overrides["flexslider_optionset.default.options|pausePlay"] = 0;
  $overrides["flexslider_optionset.default.options|randomize"] = 0;
  $overrides["flexslider_optionset.default.options|reverse"] = 0;
  $overrides["flexslider_optionset.default.options|slideshow"] = 1;
  $overrides["flexslider_optionset.default.options|smoothHeight"] = 0;
  $overrides["flexslider_optionset.default.options|thumbCaptions"] = 0;
  $overrides["flexslider_optionset.default.options|thumbCaptionsBoth"] = 0;
  $overrides["flexslider_optionset.default.options|touch"] = 1;
  $overrides["flexslider_optionset.default.options|useCSS"] = 1;
  $overrides["flexslider_optionset.default.options|video"] = 0;
  $overrides["flexslider_optionset.default.type"]["DELETED"] = TRUE;

  // Exported overrides for: image
  $overrides["image.flexslider_full.label"] = 'flexslider_full';
  $overrides["image.flexslider_thumbnail.label"] = 'flexslider_thumbnail';

  // Exported overrides for: views_view
  $overrides["views_view.flexslider_carousel.display|default|display_options|pager|options|offset"]["DELETED"] = TRUE;
  $overrides["views_view.flexslider_carousel.display|panel_pane_1|display_options|arguments"] = array(
    'field_oa_group_ref_target_id' => array(
      'id' => 'field_oa_group_ref_target_id',
      'table' => 'field_data_field_oa_group_ref',
      'field' => 'field_oa_group_ref_target_id',
      'default_action' => 'default',
      'default_argument_type' => 'og_user_groups',
      'summary' => array(
        'number_of_records' => 0,
        'format' => 'default_summary',
      ),
      'summary_options' => array(
        'items_per_page' => 25,
      ),
      'break_phrase' => TRUE,
    ),
  );
  $overrides["views_view.flexslider_carousel.display|panel_pane_1|display_options|defaults|arguments"] = FALSE;
  $overrides["views_view.flexslider_carousel.display|panel_pane_1|display_options|defaults|query"] = FALSE;
  $overrides["views_view.flexslider_carousel.display|panel_pane_1|display_options|query|options|distinct"] = TRUE;
  $overrides["views_view.flexslider_carousel.display|panel_pane_1|display_options|sorts|weight"]["DELETED"] = TRUE;

 return $overrides;
}
